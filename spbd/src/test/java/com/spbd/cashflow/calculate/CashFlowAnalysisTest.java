package com.spbd.cashflow.calculate;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;

public class CashFlowAnalysisTest {

	CashflowAnalysis cashflowAnalysis;
	@Before
	public void setUp() throws Exception {
		cashflowAnalysis=new CashflowAnalysis();
	}

	@Test
	public void testCalculations_withDiary() 
	{
		//generate test data
		cashflowAnalysis.setCashflow(generateTestDatawithDiary());
		
		//perform calculations
		cashflowAnalysis.calculateOutput();
		
		assertEquals(22.22,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueFinancialDiary(),0.1);
		assertEquals(30.00,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueBudget(),0.1);
		assertEquals(1.22,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedIncomeBusiness(),0.1);
		assertEquals(10.25,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeFiancialDiary(),0.1);
		assertEquals(5.12,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeBudget(),0.1);
		assertEquals(1.0,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedHousehold(),0.1);
		assertEquals(83.33,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeFinancialDiary(),0.1);
		assertEquals(83.33,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeBudget(),0.1);

		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueBudgetRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeBudgetRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedHouseholdRisk());
		assertEquals(Criteria.MEDRISK,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk());
		assertEquals(Criteria.MEDRISK,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeBudgetRisk());
		
		assertEquals(Criteria.SUMMARY[1],cashflowAnalysis.getCashflow().getSummary());
		
	}
	
	@Test
	public void testCalculations_withoutDiary() 
	{
		//generate test data
		cashflowAnalysis.setCashflow(generateTestDatawithoutDiary());
		
		//perform calculations
		cashflowAnalysis.calculateOutput();
		
		assertEquals(0.0,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueFinancialDiary(),0.1);
		assertEquals(30.00,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueBudget(),0.1);
		assertEquals(0.0,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedIncomeBusiness(),0.1);
		assertEquals(0.0,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeFiancialDiary(),0.1);
		assertEquals(3.2,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeBudget(),0.1);
		assertEquals(0.0,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedHousehold(),0.1);
		assertEquals(0.0,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeFinancialDiary(),0.1);
		assertEquals(0.0,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeBudget(),0.1);

		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getNetBusinessIncomeAsSalesRevenueBudgetRisk());
		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk());
		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk());
		assertEquals(Criteria.LOWRISK,cashflowAnalysis.getCashflow().getLoanRepaymentOfHouseholdIncomeBudgetRisk());
		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getDifferenceBetweenPresentAndExpectedHouseholdRisk());
		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk());
		assertEquals(Criteria.NARISK,cashflowAnalysis.getCashflow().getHouseholdExpensesOfHouseholdIncomeBudgetRisk());
		
		assertEquals(Criteria.SUMMARY[4],cashflowAnalysis.getCashflow().getSummary());
		
	}
	

	
	
	private Cashflow generateTestDatawithDiary() {

		Cashflow cashflow = new Cashflow();
		cashflow.setNumberOfDiaryMonths(1);
		cashflow.setDiaryBusinessInAverage(450);
		cashflow.setDiaryBusinessOutAverage(350);
		cashflow.setDiaryBusinessNetAverage(100);
		cashflow.setDiaryHouseholdInAverage(300);
		cashflow.setDiaryHouseholdOutAverage(250);
		cashflow.setDiaryHouseholdNetAverage(50);
		cashflow.setExpectedBusinessInAverage(1000);
		cashflow.setExpectedBusinessOutAverage(700);
		cashflow.setExpectedBusinessNetAverage(300);
		cashflow.setExpectedHouseholdInAverage(600);
		cashflow.setExpectedHouseholdOutAverage(500);
		cashflow.setExpectedHouseholdNetAverage(100);

		cashflow.setLoanAmount(1600.0);
		cashflow.setAccepted(true);
//		cashflow.setSummary("High Risk loan");
	//	loan.setDifferenceBetweenPresentAndExpectedHouseholdRisk("high");
/*		Applicant cust = new Applicant();
		cust.setId(88888);*/
/*		List<Diary> diaryList = new ArrayList<Diary>();
		Diary diary = new Diary();
		diary.setBusinessIn(10.0);
		diary.setBusinessOut(10.0);
		diary.setHouseholdIn(92.0);
		diary.setHouseholdOut(992.0);
		diary.setMonthNumber(1);
		diaryList.add(diary);
		cashflow.setDiaries(diaryList);
		cashflow.setApplicant(cust);*/
		return cashflow;

	}
	private Cashflow generateTestDatawithoutDiary() {

		Cashflow cashflow = new Cashflow();
		cashflow.setNumberOfDiaryMonths(0);
/*		cashflow.setDiaryBusinessInAverage(450);
		cashflow.setDiaryBusinessOutAverage(350);
		cashflow.setDiaryBusinessNetAverage(100);
		cashflow.setDiaryHouseholdInAverage(350);
		cashflow.setDiaryHouseholdOutAverage(250);
		cashflow.setDiaryHouseholdNetAverage(50);*/
		cashflow.setExpectedBusinessInAverage(1000);
		cashflow.setExpectedBusinessOutAverage(700);
		cashflow.setExpectedBusinessNetAverage(300);
		cashflow.setExpectedHouseholdInAverage(600);
		cashflow.setExpectedHouseholdOutAverage(500);
		cashflow.setExpectedHouseholdNetAverage(100);

		cashflow.setLoanAmount(1000.0);
		cashflow.setAccepted(true);
		return cashflow;
	}
}
