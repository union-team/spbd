package com.spbd.cashflow.managedController;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ApplicantService;
import com.spbd.cashflow.spring.service.UserService;

public class ApplicantManagedBeanTest {

	@InjectMocks
	ApplicantManagedBean applicantMB;
	
	@Mock
	ApplicantService applicantService;
	
	@Mock
	UserService userService;
	
	@Mock
	UserManagedBean userMB;
	
	ArrayList<Applicant> applicantList;
	User john;
	User neo;
	Applicant applicant1;
	Applicant applicant2;
	
	@Before
	public void setUp() {
		applicantMB = new ApplicantManagedBean();
		
		applicantService = Mockito.mock(ApplicantService.class);
		applicantMB.setApplicantService(applicantService);
		
		userService = Mockito.mock(UserService.class);
		applicantMB.setUserService(userService);
		
		userMB = Mockito.mock(UserManagedBean.class);
		applicantMB.setUserMB(userMB);
		
		applicantList = new ArrayList<Applicant>();
		john = new User();
		john.setUsername("john");
		john.setFirstName("john");
		john.setLastName("doe");
		
		neo = new User();
		neo.setUsername("neo");
		neo.setFirstName("neo");
		neo.setLastName("sir");
		
		applicant1 = new Applicant();
		applicant1.setName("sarah");
		applicant1.setDob(new Date(123432323));
		applicant1.setLoanOfficer(john);
		
		applicant2 = new Applicant();
		applicant2.setName("peter");
		applicant2.setDob(new Date(320432949));
		applicant2.setLoanOfficer(neo);
		
		applicantList.add(applicant1);
		applicantList.add(applicant2);
		
		
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * If the user only has user-access it should only be able to access it's own applicants.
	 */
	@Test
	public void test_GetApplicantList_UserOnlyAccessOwnApplicants() {
		Mockito.when(userMB.getUserLoginRole()).thenReturn("User");
		Mockito.when(userMB.getUserLogin()).thenReturn(john);
		
		Mockito.when(applicantService.getApplicants()).thenReturn(applicantList);
		
		ArrayList<Applicant> list = new ArrayList<Applicant>();
		list.add(applicant1);
		
		assertEquals(applicantMB.getApplicantList().size(),list.size());
		assertEquals(applicantMB.getApplicantList().get(0),list.get(0));
	}
	
	/**
	 * If the user has admin-access it can access all applicants.
	 */
	@Test
	public void test_GetApplicantList_AdminAccessAllApplicants() {
		Mockito.when(userMB.getUserLoginRole()).thenReturn("Admin");
		Mockito.when(userMB.getUserLogin()).thenReturn(neo);
		
		Mockito.when(applicantService.getApplicants()).thenReturn(applicantList);
		
		
		assertEquals(applicantMB.getApplicantList(),applicantList);
	}
	
	/**
	 * If the applicant is not in the DB, then adds it
	 */
	@Test
	public void test_AddApplicant_SuccessfullyAdds() {
		Mockito.doNothing().when(applicantService).addApplicant(applicant1);
		Mockito.doNothing().when(userService).addUserApplicant(john, applicant1);
	
		Mockito.when(applicantService.getApplicantByName(applicant1.getName(), applicant1.getDob())).thenReturn(null);
		
		String SUCCESS = "loans/addcashflow";
		
		assertEquals(applicantMB.addApplicant(),SUCCESS);
	}
	
	/**
	 * If the applicant already exists, returns error
	 */
	@Test
	public void test_AddApplicantAlreadyExists_ReturnsError() {
		Mockito.doNothing().when(applicantService).addApplicant(applicant1);
		Mockito.doNothing().when(userService).addUserApplicant(john, applicant1);
	
		Mockito.when(applicantService.getApplicantByName(applicant1.getName(), applicant1.getDob())).thenReturn(applicant1);
		
		String ERROR = "error";
		
		applicantMB.setName(applicant1.getName());
		applicantMB.setDob(applicant1.getDob());
		
		assertEquals(applicantMB.addApplicant(),ERROR);
	}
	
}
