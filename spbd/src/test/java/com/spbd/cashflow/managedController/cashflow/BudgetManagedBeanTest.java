package com.spbd.cashflow.managedController.cashflow;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Cashflow;

public class BudgetManagedBeanTest {
	
	@InjectMocks
	BudgetManagedBean budgetMB;
	
	@InjectMocks
	SessionDataBean sessionMB;
	
	@Before
	public void setUp() {
		budgetMB = new BudgetManagedBean();
		sessionMB = new SessionDataBean();
		sessionMB.init();
		Cashflow cashflow = new Cashflow();
		cashflow.setExpectedBusinessInAverage(10);
		cashflow.setExpectedBusinessOutAverage(10);
		cashflow.setExpectedBusinessNetAverage(0);
		cashflow.setExpectedHouseholdInAverage(20);
		cashflow.setExpectedHouseholdOutAverage(10);
		cashflow.setExpectedHouseholdNetAverage(10);
		sessionMB.getCashflowObject().setCashflow(cashflow);
		budgetMB.setSessionDataBean(sessionMB);
	}

	/**
	 * Checks that the user gets returned to review page after adding the budget
	 */
	@Test
	public void test_AddBudget_ReturnReviewScreen() {
		assertEquals(budgetMB.addBudget(),"review");
	}
	
	/**
	 * Checks that the user gets returned to an error page if the budget is null
	 */
	@Test
	public void test_AddBudgetNull_ReturnErrorScreen() {
		budgetMB.setSessionDataBean(null);
		assertEquals(budgetMB.addBudget(),"error");
	}
	
}
