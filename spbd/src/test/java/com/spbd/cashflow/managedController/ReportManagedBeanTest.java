package com.spbd.cashflow.managedController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.spbd.cashflow.managedController.LoanOfficerInfo;
import com.spbd.cashflow.managedController.ReportManagedBean;
import com.spbd.cashflow.managedController.report.MonthlyReport;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ReportService;

import junit.framework.TestCase;

import org.mockito.*;

public class ReportManagedBeanTest extends TestCase {
	
	@InjectMocks
	private ReportManagedBean reportManagedBean;

	@Mock
	private ReportService reportService;
	
	private User user;
	private Cashflow cashflow1;
	
	@Before
	protected void setUp() throws Exception {
		reportManagedBean = new ReportManagedBean();
		reportService = Mockito.mock(ReportService.class);
		reportManagedBean.setReportService(reportService);
		MockitoAnnotations.initMocks(this);
		user = Mockito.mock(User.class);
		cashflow1 = Mockito.mock(Cashflow.class);

	}
	@Test
	public void testShowPastThreeMonthsRecords() {
		Date endDate = new Date();
		Date startDate = MonthlyReport.subtractMonth(endDate);
		double loanAmount=0;
		List<Cashflow> cashflowList = reportManagedBean.getCashflowList();
		for (Cashflow c: cashflowList) {
			if (c.getDateSubmitted().after(startDate)&&c.getDateSubmitted().before(endDate))
				loanAmount += c.getLoanAmount();
		}
		reportManagedBean.showPastThreeMonthsRecords();
		assertEquals(reportManagedBean.getLoanAmount0(), loanAmount);
		loanAmount = 0;
		endDate = startDate;
		startDate = MonthlyReport.subtractMonth(endDate);
		for (Cashflow c: cashflowList) {
			if (c.getDateSubmitted().after(startDate)&&c.getDateSubmitted().before(endDate))
				loanAmount += c.getLoanAmount();
		}
		assertEquals(reportManagedBean.getLoanAmount1(), loanAmount);
		loanAmount = 0;
		endDate = startDate;
		startDate = MonthlyReport.subtractMonth(endDate);
		for (Cashflow c: cashflowList) {
			if (c.getDateSubmitted().after(startDate)&&c.getDateSubmitted().before(endDate))
				loanAmount += c.getLoanAmount();
		}
		assertEquals(reportManagedBean.getLoanAmount2(), loanAmount);
	}
	@Test
	public void testShowTotalLoanAmount() {
		double amount = 0;
		List<Cashflow> cashflowList = new ArrayList<Cashflow>();
		cashflowList.add(cashflow1);
		reportManagedBean.setCashflowList(cashflowList);
		List<Cashflow> beanList = reportManagedBean.getCashflowList();
		for (Cashflow c: beanList) {
			amount += c.getLoanAmount();
		}
		Mockito.when(reportService.getTotalLoanAmount()).thenReturn(0);
		reportManagedBean.showTotalLoanAmount();
		assertEquals((int)amount, reportManagedBean.getLoanAmount());
	}
	@Test
	public void testShowApplicantsByUser() {
		List <Applicant> applicantList = new ArrayList<Applicant>();
		reportManagedBean.setUser(user);
		Mockito.when(reportService.getApplicantsByLoanOfficer(user)).thenReturn(applicantList);
		reportManagedBean.showApplicantsByUser(null);
		assertEquals(reportManagedBean.getApplicantByUser(), applicantList);
        Applicant app = Mockito.mock(Applicant.class);
        applicantList.add(app);
        user.setApplicants(applicantList);
		reportManagedBean.setUser(user);
		reportManagedBean.showApplicantsByUser(null);
		assertEquals(reportManagedBean.getApplicantByUser(), applicantList);
	}
	@Test
	public void testShowCashflowsByUser() {
		List <Cashflow> cashflowList = new ArrayList<Cashflow>();
		Mockito.when(reportService.getCashflowByUser(user)).thenReturn(cashflowList);
		reportManagedBean.setUser(user);
		reportManagedBean.showCashflowsByUser(null);
		assertEquals(reportManagedBean.getCashflowByUser(), cashflowList);
        Applicant app = Mockito.mock(Applicant.class);
        List <Applicant> applicantList = new ArrayList<Applicant>();
        applicantList.add(app);
        user.setApplicants(applicantList);
		reportManagedBean.setUser(user);
		reportManagedBean.showCashflowsByUser(null);
		assertEquals(reportManagedBean.getCashflowByUser(), cashflowList);
	}
	@Test
	public void testShowApplicantList() {
        Applicant app = Mockito.mock(Applicant.class);
        app.setName("aName");
        List <Applicant> applicantList = new ArrayList<Applicant>();
        applicantList.add(app);
        user.setApplicants(applicantList);
        List<User> userList = new ArrayList<User>();
        userList.add(user);
        reportManagedBean.setUserList(userList);
        Mockito.when(reportService.getApplicantList()).thenReturn(applicantList);
		reportManagedBean.showApplicantList();
		boolean exists = false;
		for (Applicant applicant:reportManagedBean.getApplicantList()) {
			if (applicant.equals(app))
				exists = true;
		}
		assertEquals(exists,true);
	}
	@Test
	public void testShowLoanInfo() {
		User user2 = Mockito.mock(User.class);
		Applicant applicant = Mockito.mock(Applicant.class);
		List<Applicant> appList = new ArrayList<Applicant>();
		appList.add(applicant);
		user2.setApplicants(appList);
		List<User> userList = new ArrayList<User>();
		userList.add(user);
		userList.add(user2);
		List<Cashflow> cashflowList = new ArrayList<Cashflow>();
		cashflowList.add(cashflow1);
		applicant.setCashflows(cashflowList);
		Mockito.when(reportService.getCashflowByUser(user)).thenReturn(new ArrayList<Cashflow>());
		Mockito.when(reportService.getCashflowByUser(user2)).thenReturn(cashflowList);
		reportManagedBean.setUserList(userList);
		reportManagedBean.showLoanInfo();
		for (LoanOfficerInfo loi:reportManagedBean.getOfficerInfoList()) {
			if (loi.getName().equals(user.getFirstName()+" "+user.getLastName())) {
				assertEquals(loi.getTotalAmount(),0);
			}
			else if (loi.getName().equals(user2.getFirstName()+" "+user2.getLastName())) {
				assertEquals(loi.getTotalAmount(),cashflow1.getLoanAmount());
			}
		}
	}


}
