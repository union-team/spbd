package com.spbd.cashflow.managedController.cashflow;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.spbd.cashflow.managedController.SessionDataBean;

public class AddCashflowManagedBeanTest {

	@InjectMocks
	AddCashflowManagedBean addCashflowMB;
	
	@InjectMocks
	SessionDataBean sessionDataBean;
	
	@Before
	public void setUp() {
		addCashflowMB = new AddCashflowManagedBean();
		sessionDataBean = new SessionDataBean();
		addCashflowMB.setSessionDataBean(sessionDataBean);
	}
	
	/**
	 * Checks if the loan amount is greater than 1500, if so return diary page
	 */
	@Test
	public void test_AddCashflowAmountOver1500_ReturnsDiaryPage() {
		addCashflowMB.setLoanAmount(1501);
		
		assertEquals(addCashflowMB.addCashflow(),"diary");
	}
	
	/**
	 * Checks if the loan amount is less than or equal to 1500, if so return budget page
	 */
	@Test
	public void test_AddCashflowAmountUnder1500_ReturnsBudgetPage() {
		addCashflowMB.setLoanAmount(1499);
		
		assertEquals(addCashflowMB.addCashflow(),"budget");
	}
}
