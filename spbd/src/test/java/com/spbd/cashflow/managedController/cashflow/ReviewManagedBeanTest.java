package com.spbd.cashflow.managedController.cashflow;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Cashflow;

public class ReviewManagedBeanTest {
	
	@InjectMocks
	ReviewManagedBean reviewMB;
	
	@InjectMocks
	SessionDataBean sessionMB;
	
	@Before
	public void setUp() {
		reviewMB = new ReviewManagedBean();
		sessionMB = new SessionDataBean();
		sessionMB.init();
		Cashflow cashflow = new Cashflow();
		cashflow.setExpectedBusinessInAverage(11);
		cashflow.setExpectedBusinessOutAverage(10);
		cashflow.setExpectedBusinessNetAverage(1);
		cashflow.setExpectedHouseholdInAverage(20);
		cashflow.setExpectedHouseholdOutAverage(10);
		cashflow.setExpectedHouseholdNetAverage(10);
		cashflow.setDiaryBusinessInAverage(11);
		cashflow.setDiaryBusinessOutAverage(10);
		cashflow.setDiaryBusinessNetAverage(1);
		cashflow.setDiaryHouseholdInAverage(11);
		cashflow.setDiaryHouseholdOutAverage(10);
		cashflow.setDiaryHouseholdNetAverage(1);
		sessionMB.getCashflowObject().setCashflow(cashflow);
		reviewMB.setSessionDataBean(sessionMB);
	}

	/**
	 * Checks that the cashflow has populated the screen successfully.
	 * Sample cashflow has all values greater than 0.
	 */
	@Test
	public void test_Populate_ReturnsPopulatedCashflow() {
		reviewMB.populate();
		
		assertTrue(reviewMB.getBudgetBusinessIn()>0);
		assertTrue(reviewMB.getBudgetBusinessOut()>0);
		assertTrue(reviewMB.getBudgetBusinessNet()>0);
		
		assertTrue(reviewMB.getBudgetHouseholdIn()>0);
		assertTrue(reviewMB.getBudgetHouseholdOut()>0);
		assertTrue(reviewMB.getBudgetHouseholdNet()>0);
		
		assertTrue(reviewMB.getPastBusinessIn()>0);
		assertTrue(reviewMB.getPastBusinessOut()>0);
		assertTrue(reviewMB.getPastBusinessNet()>0);
		
		assertTrue(reviewMB.getPastHouseholdIn()>0);
		assertTrue(reviewMB.getPastHouseholdOut()>0);
		assertTrue(reviewMB.getPastHouseholdNet()>0);
	}
	
	/**
	 * Checks that the cashflow has populated the screen successfully.
	 * Default cashflow has all values set to 0.0.
	 */
	@Test
	public void test_NotPopulating_ReturnsEmptyCashflow() {		
		assertTrue(reviewMB.getBudgetBusinessIn()==0.0);
		assertTrue(reviewMB.getBudgetBusinessOut()==0.0);
		assertTrue(reviewMB.getBudgetBusinessNet()==0.0);
		
		assertTrue(reviewMB.getBudgetHouseholdIn()==0.0);
		assertTrue(reviewMB.getBudgetHouseholdOut()==0.0);
		assertTrue(reviewMB.getBudgetHouseholdNet()==0.0);
		
		assertTrue(reviewMB.getPastBusinessIn()==0.0);
		assertTrue(reviewMB.getPastBusinessOut()==0.0);
		assertTrue(reviewMB.getPastBusinessNet()==0.0);
		
		assertTrue(reviewMB.getPastHouseholdIn()==0.0);
		assertTrue(reviewMB.getPastHouseholdOut()==0.0);
		assertTrue(reviewMB.getPastHouseholdNet()==0.0);
	}
	
}
