package com.spbd.cashflow.managedController;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;







import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserService;

public class UserManagedBeanTest{

	@InjectMocks
	private UserManagedBean userManagedBean;

	@Mock
	private UserService userService;
	
	private User user;	
		
	@Before
	public void setUp() throws Exception {
			
		userManagedBean = new UserManagedBean();
		userService = Mockito.mock(UserService.class);
		userManagedBean.setUserService(userService);
		MockitoAnnotations.initMocks(this);
		user = Mockito.mock(User.class);			
	}	


	@Test
	public void testDelete() {
		
		long testId = 1;
							
		User user1 = new User();
		user1 = userService.getUserById(testId);
		
		Mockito.when(userService.getUserById(testId)).thenReturn(user1);
		
		assertNull("user not exist",user1);
		
		userService.deleteUser(user1);
				
	}
	
	@Test
	public void userDetailsByUsername() {
		
		String testUsername = "test";
		User user2 = new User();
		
		user2 = userService.getUserByUserName(testUsername);
				
		Mockito.when(userService.getUserByUserName(testUsername)).thenReturn(user2);
		
		assertEquals(userService.getUserByUserName(testUsername), user2);

		assertNull("user not exist",user2);
		
	}
	
	@Test
	public void testUserDetails() {

		long testId = 5;
		User user3 = new User();
		
		user3 = userService.getUserById(testId);
		
		Mockito.when(userService.getUserById(testId)).thenReturn(user3);
					
		assertNull("user not exist",user3);
		
	}	
	
	@Test
	public void ValidateEmailAddress() {
		
		String email = "test@yahoo.com";

		User user1 = new User();
		
		assertNull(user1.getEmailAddress());
		user1.setEmailAddress(email);
		assertEquals(email,user1.getEmailAddress());
		
		}

	@Test
	public void ValidateUsernameDuplicate() {
				
		String username = "test";

		User user2 = new User();
		
		assertNull(user2.getUsername());
		user2.setUsername(username);
		assertEquals(username,user2.getUsername());			
			
	}

	@Test
	public void PasswordValidator() {

		String password = "password";

		User user3 = new User();
		
		assertNull(user3.getPassword());
		user3.setPassword(password);
		assertEquals(password,user3.getPassword());		
	}
	
	@Test
	public void MobileNumberValidator() {

		String mobile = "+62410866788";

		User user4 = new User();
		
		assertNull(user4.getMobileNnumber());
		user4.setMobileNnumber(mobile);
		assertEquals(mobile,user4.getMobileNnumber());	
		
	}
	
}
