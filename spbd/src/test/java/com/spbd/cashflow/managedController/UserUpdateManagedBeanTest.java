package com.spbd.cashflow.managedController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import junit.framework.TestCase;

import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserService;

public class UserUpdateManagedBeanTest extends TestCase{

	private User user;
	private UserService userService;
		
	
	public void setUp() throws Exception {
			
		user = new User();
			
	}	

	@Test
	public void testUpdateUser() throws ParseException {
		
		long testId = 1;
		assertEquals(0,0,0);
		user.setId(testId);
		assertEquals("Id not equal the expected one",testId,user.getId(), 0);
		
		String testUsername = "test";
		assertNull("Username is Null",user.getUsername());
		user.setUsername(testUsername);
		assertEquals("Usename not equal the expected one", testUsername,user.getUsername());
		
		String testPassword = "123";
		assertNull("Password is Null",user.getPassword());
		user.setPassword(testPassword);
		assertEquals("Password not equal the expected one", testPassword,user.getPassword());
		
		String testFirstName = "Motasem";
		assertNull("FirstName is Null",user.getFirstName());
		user.setFirstName(testFirstName);
		assertEquals("FirstName not equal the expected one", testFirstName,user.getFirstName());
		
		String testLastName = "Naffa";
		assertNull("LastName is Null",user.getLastName());
		user.setLastName(testLastName);
		assertEquals("LastName not equal the expected one", testLastName,user.getLastName());
		
		DateFormat formatter ; 
		Date testDateOfBith ; 
		formatter = new SimpleDateFormat("dd-MMM-yyyy");
		testDateOfBith = formatter.parse("05-Aug-1987");
		
		assertNull("Date of Birth is Null",user.getDateOfBith());
		user.setDateOfBith(testDateOfBith);
		assertEquals("Date Of Bith not equal the expected one", testDateOfBith,user.getDateOfBith());
		
		String testGender = "Male";
		assertNull("Gender is Null",user.getGender());
		user.setGender(testGender);
		assertEquals("Gender not equal the expected one", testGender,user.getGender());
		
		String testEmailAddress = "test@yahoo.com";
		assertNull("Email Address is Null",user.getEmailAddress());
		user.setEmailAddress(testEmailAddress);
		assertEquals("Email Address not equal the expected one", testEmailAddress,user.getEmailAddress());
		
		String testMobileNnumber = "+61 4 10855677";
		assertNull("Mobile Number is Null",user.getMobileNnumber());
		user.setMobileNnumber(testMobileNnumber);
		assertEquals("Mobile Nnumber not equal the expected one", testMobileNnumber,user.getMobileNnumber());
		
		String testCountry = "Australia";
		assertNull("Country is Null",user.getCountry());
		user.setCountry(testCountry);
		assertEquals("Country not equal the expected one", testCountry,user.getCountry());
		
		String testAddress = "Strathfield";
		assertNull("Address is Null",user.getAddress());
		user.setAddress(testAddress);
		assertEquals("Address not equal the expected one", testAddress,user.getAddress());	
		
		// If all fields not null, we will add a new user successfully
		
	}
	
	@Test
	public void testDelete(long id) {
		
		long testId = id;
		assertEquals(0,0,0);
		user.setId(testId);
		assertEquals("ID not equal the expected one", testId,user.getId(), 0);
		
		User user = new User();
		
		user = userService.getUserById(id);
		assertNull("user not exist",user);
		
		// User if exist will be delete successfully
		
	}
	
	@Test
	public void userDetailsByUsername(String username) {
		
		String testUsername = username;
		assertNull("Username is Null",user.getUsername());
		user.setUsername(testUsername);
		assertEquals("Usename not equal the correct one", testUsername,user.getUsername());
		
		User user = new User();
		
		user = userService.getUserByUserName(username);
		assertNull("user not exist",user);
		
		// User if exist will get his details successfully
		
	}
	
	@Test
	public void testUserDetails(long id) {

		long testId = id;
		assertEquals(0,0,0);
		user.setId(testId);
		assertEquals("ID not equal the expected one", testId,user.getId(), 0);
		
		User user = new User();
		
		user = userService.getUserById(id);
		assertNull("user not exist",user);
		
		// User if exist will get his details successfully
		
	}	
	
	@Test
	public void ValidateEmailAddress() {
		
		String email = "test@yahoo.com";

		User user1 = new User();
		
		assertNull(user1.getEmailAddress());
		user1.setEmailAddress(email);
		assertEquals(email,user1.getEmailAddress());
		
		}

	@Test
	public void ValidateUsernameDuplicate() {
				
		String username = "test";

		User user2 = new User();
		
		assertNull(user2.getUsername());
		user2.setUsername(username);
		assertEquals(username,user2.getUsername());			
			
	}

	@Test
	public void PasswordValidator() {

		String password = "password";

		User user3 = new User();
		
		assertNull(user3.getPassword());
		user3.setPassword(password);
		assertEquals(password,user3.getPassword());		
	}
	
	@Test
	public void MobileNumberValidator() {

		String mobile = "+62410866788";

		User user4 = new User();
		
		assertNull(user4.getMobileNnumber());
		user4.setMobileNnumber(mobile);
		assertEquals(mobile,user4.getMobileNnumber());	
		
	}
	
}
