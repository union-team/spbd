package com.spbd.cashflow.managedController;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.spbd.cashflow.managedController.UserReportManagedBean;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserReportService;

public class UserReportManagedBeanTest {

	
	@InjectMocks
	private UserReportManagedBean userReportManagedBean;

	@Mock
	private UserReportService userReportService;
	
	private User user;
	private Cashflow cashflow1;
	private Applicant applicant;
	
	public UserReportManagedBeanTest() {
	}

	@Before
	public void setUp() throws Exception {
		userReportManagedBean = new UserReportManagedBean();
		userReportService = Mockito.mock(UserReportService.class);
		userReportManagedBean.setUserReportService(userReportService);
		MockitoAnnotations.initMocks(this);
		user = Mockito.mock(User.class);
		userReportManagedBean.setUser(user);
		cashflow1 = Mockito.mock(Cashflow.class);
		applicant = Mockito.mock(Applicant.class);
	}

	@Test
	public final void testApplicantList() {
		List<Applicant> applicantList = new ArrayList<Applicant>();
		applicantList.add(applicant);
		Mockito.when(userReportService.applicantList(user)).thenReturn(applicantList);
		userReportManagedBean.getApplicants();
		assertEquals(userReportManagedBean.getApplicantList(),applicantList);
	}

	@Test
	public final void testCashflowList() {
		List<Cashflow> cashflowList = new ArrayList<Cashflow>();
		cashflowList.add(cashflow1);
		Mockito.when(userReportService.cashflowList(userReportService.applicantList(user))).thenReturn(cashflowList);
		userReportManagedBean.getCashflows();
		assertEquals(userReportManagedBean.getCashflowList(),cashflowList);
	}

	@Test
	public final void testTotalLoanAmount() {
		Cashflow cashflow2 = Mockito.mock(Cashflow.class);
		Cashflow cashflow3 = Mockito.mock(Cashflow.class);
		Cashflow cashflow4 = Mockito.mock(Cashflow.class);
		List<Cashflow> cashflowList = new ArrayList<Cashflow>();
		cashflowList.add(cashflow2);
		cashflowList.add(cashflow3);
		cashflowList.add(cashflow4);		
		userReportManagedBean.setCashflowList(cashflowList);
		double totalLoanAmount = cashflow2.getLoanAmount()+cashflow3.getLoanAmount()+cashflow3.getLoanAmount();
		assertEquals((int)userReportManagedBean.totalLoanAmount(),(int)totalLoanAmount);
	}

	@Test
	public final void testThisMonthLoanAmount() {
		Cashflow cashflow2 = new Cashflow();
		Cashflow cashflow3 = new Cashflow();
		Cashflow cashflow4 = new Cashflow();
		cashflow2.setLoanAmount(10);
		cashflow3.setLoanAmount(20);
		cashflow4.setLoanAmount(30);
		Date d0 = new Date();
		Date d1 = new Date();
		Date d2 = new Date();
		d1.setMonth(2);
		cashflow2.setDateSubmitted(d0);
		cashflow3.setDateSubmitted(d1);
		cashflow4.setDateSubmitted(d2);
		cashflow2.setAccepted(true);
		cashflow3.setAccepted(true);
		cashflow4.setAccepted(true);
//		Cashflow cashflow2 = Mockito.mock(Cashflow.class);
//		Cashflow cashflow3 = Mockito.mock(Cashflow.class);
//		Cashflow cashflow4 = Mockito.mock(Cashflow.class);
		List<Cashflow> cashflowList = new ArrayList<Cashflow>();
		cashflowList.add(cashflow2);
		cashflowList.add(cashflow3);
		cashflowList.add(cashflow4);		
		userReportManagedBean.setCashflowList(cashflowList);
		double totalLoanAmount = 0;
		Date date = new Date();
		for (Cashflow c:cashflowList) {
			if(c.getDateSubmitted().getMonth()==date.getMonth())
				totalLoanAmount +=c.getLoanAmount();
		}
		Mockito.when(userReportService.cashflowList(userReportService.applicantList(user))).thenReturn(cashflowList);
		userReportManagedBean.thisMonthLoanAmount();
		int a = (int)userReportManagedBean.thisMonthLoanAmount;
		assertEquals((int)totalLoanAmount, a, 0.1);
	}
}
