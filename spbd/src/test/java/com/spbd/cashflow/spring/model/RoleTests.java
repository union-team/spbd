package com.spbd.cashflow.spring.model;

import org.junit.Test;

import junit.framework.TestCase;

	public class RoleTests extends TestCase{
	
	private Role role;
	
	
	protected void setUp() throws Exception {
		
		role = new Role();
	}

	
	@Test
	public void testSetAndGetRole_id(){
	
	int testRole_id = 1;
	assertEquals(0,0,0);
	role.setRole_id(testRole_id);
	assertEquals(testRole_id,role.getRole_id(), 0);
	}
	
	@Test
	public void testSetAndGetRoleName(){
		
		String testRoleName = "aRoleName";
		assertNull(role.getRoleName());
		role.setRoleName(testRoleName);
		assertEquals(testRoleName,role.getRoleName());
	}
	
	@Test
	public void testSetAndGetDescription(){
		
		String testDescription = "aDescription";
		assertNull(role.getDescription());
		role.setDescription(testDescription);
		assertEquals(testDescription,role.getDescription());
	}


}