package com.spbd.cashflow.spring.model;

import org.junit.Test;

import junit.framework.TestCase;

public class UsersAndRolesTests extends TestCase{
	
	
	private UsersAndRoles usersAndRoles;
	
	
	protected void setUp() throws Exception {
		
		usersAndRoles = new UsersAndRoles();
	}
	
	@Test
	public void testSetAndGetId(){
		
		long testId = 1;
		assertEquals(0,0,0);
		usersAndRoles.setId(testId);
		assertEquals(testId,usersAndRoles.getId(), 0);
	}

	@Test
	public void testSetAndGetRole_id(){
		
	int testRole_id = 1;
	assertEquals(0,0,0);
	usersAndRoles.setRole_id(testRole_id);
	assertEquals(testRole_id,usersAndRoles.getRole_id(), 0);
	}


}
