package com.spbd.cashflow.spring.model;

import com.spbd.cashflow.spring.model.Diary;

import junit.framework.TestCase;

public class DiaryTest extends TestCase {

    private Diary diary;

    protected void setUp() throws Exception {
        diary = new Diary();
    }

    public void testSetAndGetId() {
        long testId = 1;
        assertEquals(0, 0, 0);
        diary.setId(testId);
        assertEquals(testId, diary.getId());
    }
    
    public void testSetAndGetMonthNumber() {
        int testMonthNumber = 1;
        assertEquals(0, 0, 0); 
        diary.setMonthNumber(testMonthNumber);
        assertEquals(testMonthNumber, diary.getMonthNumber(), 0);
    }
    
    public void testSetAndGetBusinessIn() {
        double testBusinessIn = 1.2;
        assertEquals(0, 0, 0); 
        diary.setBusinessIn(testBusinessIn);
        assertEquals(testBusinessIn, diary.getBusinessIn(), 0);
    }
    
    public void testSetAndGetBusinessOut() {
        double testBusinessOut = 1.2;
        assertEquals(0, 0, 0); 
        diary.setBusinessOut(testBusinessOut);
        assertEquals(testBusinessOut, diary.getBusinessOut(), 0);
    }
    
    public void testSetAndGetHouseholdIn() {
        double testHouseholdIn = 1.2;
        assertEquals(0, 0, 0); 
        diary.setHouseholdIn(testHouseholdIn);
        assertEquals(testHouseholdIn, diary.getHouseholdIn(), 0);
    }
    
    public void testSetAndGetHouseholdOut() {
        double testHouseholdOut = 1.2;
        assertEquals(0, 0, 0); 
        diary.setHouseholdOut(testHouseholdOut);
        assertEquals(testHouseholdOut, diary.getHouseholdOut(), 0);
    }

 
  
}
