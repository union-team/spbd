package com.spbd.cashflow.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;
import com.spbd.cashflow.spring.model.User;

import junit.framework.TestCase;

public class ApplicantTest extends TestCase {

    private Applicant applicant;
    private List<Cashflow> cashflows;
    
    private static int CASHFLOW_COUNT = 1;
    
    private static long A_ID = 1;
    private static double A_LOANAMOUNT = new Double(1.1);
    private static Date A_DATESUBMITTED = new Date();
    private static double A_WEEKLYREPAYEMNT = new Double(1.1);
    private static int A_WEEKSTOPAY = 4;
    private static String A_SUMMARY = "Good";
    private static Boolean A_ACCEPTED = true;
    private static int A_NUMBEROFDIARYMONTHS = 6;
    private static double A_DIARYBUSINESSINAVERAGE = new Double(1.1);
    private static double A_DIARYBUSINESSOUTAVERAGE = new Double(1.1);
    private static double A_DIARYBUSINESSNETAVERAGE = new Double(3.6);
    private static double A_DIARYHOUSEHOLDINAVERAGE = new Double(3.4);
    private static double A_DIARYHOUSEHOLDOUTAVERAGE = new Double(3.5);
    private static double A_DIARYHOUSEHOLDNETAVERAGE = new Double(3.6);
    private static double A_EXPECTEDBUSINESSINAVERAGE = new Double(3.3);
    private static double A_EXPECTEDBUSINESSOUTAVERAGE = new Double(3.3);
    private static double A_EXPECTEDBUSINESSNETAVERAGE = new Double(3.3);
    private static double A_EXPECTEDHOUSEHOLDINAVERAGE = new Double(3.3);
    private static double A_EXPECTEDHOUSEHOLDOUTAVERAGE = new Double(3.3);
    private static double A_EXPECTEDHOUSEHOLDNETAVERAGE = new Double(3.3);
    private static double A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARY = new Double(6.7);
    private static String A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARYRISK = "aString";
    private static double A_NETBUSINESSINCOMEASSALESREVENUEBUDGET = new Double(3.4);
    private static String A_NETBUSINESSINCOMEASSALESREVENUEBUDGETRISK = "aString";
    private static double A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESS = new Double(3.4);
    private static String A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESSRISK = "aString";
    private static double A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARY = new Double(3.4);
    private static String A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARYRISK = "aString";
    private static double A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGET = new Double(4.4);
    private static String A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGETRISK = "aString";
    private static double A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSHOLD = new Double(3.4);
    private static String A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSEHOLDRISK = "aString";
    private static double A_HOUSEHOLDEXPENSEFINANCIALDIARY = new Double(3.4);
    private static String A_HOUSEHOLDEXPENSEFINANCIALDIARYRISK = "aString";
    private static double A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGET = new Double(3.4);
    private static String A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGETRISK = "aString";
    private static List<Diary> A_DIARIES;
 
 	


    protected void setUp() throws Exception {
        applicant = new Applicant();
        cashflows = new ArrayList<Cashflow>();
        
        Cashflow cf = new Cashflow();
        cf.setId(A_ID);
        cf.setAccepted(A_ACCEPTED);
        cf.setApplicant(applicant);
        cf.setDateSubmitted(A_DATESUBMITTED);
        cf.setDiaries(A_DIARIES);
        cf.setDiaryBusinessInAverage(A_DIARYBUSINESSINAVERAGE);
        cf.setDiaryBusinessNetAverage(A_DIARYBUSINESSNETAVERAGE);
        cf.setDiaryBusinessOutAverage(A_DIARYBUSINESSOUTAVERAGE);
        cf.setDiaryHouseholdInAverage(A_DIARYHOUSEHOLDINAVERAGE);
        cf.setDiaryHouseholdNetAverage(A_DIARYHOUSEHOLDNETAVERAGE);
        cf.setDiaryHouseholdOutAverage(A_DIARYHOUSEHOLDOUTAVERAGE);
        cf.setDifferenceBetweenPresentAndExpectedHousehold(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSHOLD);
        cf.setDifferenceBetweenPresentAndExpectedHouseholdRisk(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSEHOLDRISK);
        cf.setDifferenceBetweenPresentAndExpectedIncomeBusiness(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESS);
        cf.setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESSRISK);
        cf.setExpectedBusinessInAverage(A_EXPECTEDBUSINESSINAVERAGE);
        cf.setExpectedBusinessNetAverage(A_EXPECTEDBUSINESSNETAVERAGE);
        cf.setExpectedBusinessOutAverage(A_EXPECTEDBUSINESSOUTAVERAGE);
        cf.setExpectedHouseholdInAverage(A_EXPECTEDHOUSEHOLDINAVERAGE);
        cf.setExpectedHouseholdNetAverage(A_EXPECTEDHOUSEHOLDNETAVERAGE);
        cf.setExpectedHouseholdOutAverage(A_EXPECTEDHOUSEHOLDOUTAVERAGE);
        cf.setHouseholdExpensesOfHouseholdIncomeBudget(A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGET);
        cf.setHouseholdExpensesOfHouseholdIncomeBudgetRisk(A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGETRISK);
        cf.setHouseholdExpensesOfHouseholdIncomeFinancialDiary(A_HOUSEHOLDEXPENSEFINANCIALDIARY);
        cf.setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(A_HOUSEHOLDEXPENSEFINANCIALDIARYRISK);
        cf.setLoanAmount(A_LOANAMOUNT);
        cf.setLoanRepaymentOfHouseholdIncomeBudget(A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGET);
        cf.setLoanRepaymentOfHouseholdIncomeBudgetRisk(A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGETRISK);
        cf.setLoanRepaymentOfHouseholdIncomeFiancialDiary(A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARY);
        cf.setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARYRISK);
        cf.setNetBusinessIncomeAsSalesRevenueBudget(A_NETBUSINESSINCOMEASSALESREVENUEBUDGET);
        cf.setNetBusinessIncomeAsSalesRevenueBudgetRisk(A_NETBUSINESSINCOMEASSALESREVENUEBUDGETRISK);
        cf.setNetBusinessIncomeAsSalesRevenueFinancialDiary(A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARY);
        cf.setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARYRISK);
        cf.setNumberOfDiaryMonths(A_NUMBEROFDIARYMONTHS);
        cf.setSummary(A_SUMMARY);
        cf.setWeeklyRepayment(A_WEEKLYREPAYEMNT);
        cf.setWeeksToPay(A_WEEKSTOPAY);
        
        cashflows.add(cf);
        
        applicant.setCashflows(cashflows);
    }

    public void testSetAndGetId() {
        long testId = 1;
        assertEquals(0, 0, 0);
        applicant.setId(testId);
        assertEquals(testId, applicant.getId());
    }
    
    public void testSetAndGetName() {
        String testName = "aName";
        assertNull(applicant.getName());
        applicant.setName(testName);
        assertEquals(testName, applicant.getName());
    }
    
    public void testSetAndGetLoanOfficer() {
        User testLoanOfficer = new User();;
        assertNull(applicant.getLoanOfficer());
        applicant.setLoanOfficer(testLoanOfficer);
        assertEquals(testLoanOfficer, applicant.getLoanOfficer());
    }
    
    public void testGetCashflowsWithNoCashflows() {
    	Applicant empty = new Applicant();
    	assertEquals(empty.getCashflows().size(), 0);
    }
    
    public void testGetCashflows() {
    	List<Cashflow> test = applicant.getCashflows();
    	assertNotNull(test);
    	assertEquals(CASHFLOW_COUNT, test.size());
    	
    	Cashflow app = test.get(0);
    	assertEquals(A_ID, app.getId());
    	assertEquals(A_ACCEPTED, app.getAccepted());
    	assertEquals(applicant, app.getApplicant());
    	assertEquals(A_DATESUBMITTED, app.getDateSubmitted());
    	assertEquals(A_DIARIES, app.getDiaries());
    	assertEquals(A_DIARYBUSINESSINAVERAGE, app.getDiaryBusinessInAverage());
    	assertEquals(A_DIARYBUSINESSNETAVERAGE, app.getDiaryBusinessNetAverage());
    	assertEquals(A_DIARYBUSINESSOUTAVERAGE, app.getDiaryBusinessOutAverage());
    	assertEquals(A_DIARYHOUSEHOLDINAVERAGE, app.getDiaryHouseholdInAverage());
    	assertEquals(A_DIARYHOUSEHOLDNETAVERAGE, app.getDiaryHouseholdNetAverage());
    	assertEquals(A_DIARYHOUSEHOLDOUTAVERAGE, app.getDiaryHouseholdOutAverage());
    	assertEquals(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSHOLD, app.getDifferenceBetweenPresentAndExpectedHousehold());
    	assertEquals(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDHOUSEHOLDRISK, app.getDifferenceBetweenPresentAndExpectedHouseholdRisk());
    	assertEquals(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESS, app.getDifferenceBetweenPresentAndExpectedIncomeBusiness());
    	assertEquals(A_DIFFERENCEBETWEENPRESENTANDEXPECTEDINCOMEBUSINESSRISK, app.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk());
    	assertEquals(A_EXPECTEDBUSINESSINAVERAGE, app.getExpectedBusinessInAverage());
    	assertEquals(A_EXPECTEDBUSINESSNETAVERAGE, app.getExpectedBusinessNetAverage());
    	assertEquals(A_EXPECTEDBUSINESSOUTAVERAGE, app.getExpectedBusinessOutAverage());
    	assertEquals(A_EXPECTEDHOUSEHOLDINAVERAGE, app.getExpectedHouseholdInAverage());
    	assertEquals(A_EXPECTEDHOUSEHOLDNETAVERAGE, app.getExpectedHouseholdNetAverage());
    	assertEquals(A_EXPECTEDHOUSEHOLDOUTAVERAGE, app.getExpectedHouseholdOutAverage());
    	assertEquals(A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGET, app.getHouseholdExpensesOfHouseholdIncomeBudget());
    	assertEquals(A_HOUSEHOLDEXPENSEOFHOUSEHOLDBUDGETRISK, app.getHouseholdExpensesOfHouseholdIncomeBudgetRisk());
    	assertEquals(A_HOUSEHOLDEXPENSEFINANCIALDIARY, app.getHouseholdExpensesOfHouseholdIncomeFinancialDiary());
    	assertEquals(A_HOUSEHOLDEXPENSEFINANCIALDIARYRISK, app.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk());
    	assertEquals(A_LOANAMOUNT, app.getLoanAmount());
    	assertEquals(A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGET, app.getLoanRepaymentOfHouseholdIncomeBudget());
    	assertEquals(A_LOANREPAYMENTOFHUSHOLDINCOMEBUDGETRISK, app.getLoanRepaymentOfHouseholdIncomeBudgetRisk());
    	assertEquals(A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARY, app.getLoanRepaymentOfHouseholdIncomeFiancialDiary());
    	assertEquals(A_LOANREPAYMENTOFHOUSEHOLDINCOMEFINANCIALDIARYRISK, app.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk());
    	assertEquals(A_NETBUSINESSINCOMEASSALESREVENUEBUDGET, app.getNetBusinessIncomeAsSalesRevenueBudget());
    	assertEquals(A_NETBUSINESSINCOMEASSALESREVENUEBUDGETRISK, app.getNetBusinessIncomeAsSalesRevenueBudgetRisk());
    	assertEquals(A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARY, app.getNetBusinessIncomeAsSalesRevenueFinancialDiary());
    	assertEquals(A_NETBUSINESSINCOMEASSALESREVENUEFINANCIALDIARYRISK, app.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk());
    	assertEquals(A_NUMBEROFDIARYMONTHS, app.getNumberOfDiaryMonths());
    	assertEquals(A_SUMMARY, app.getSummary());
    	assertEquals(A_WEEKLYREPAYEMNT, app.getWeeklyRepayment());
    	assertEquals(A_WEEKSTOPAY, app.getWeeksToPay());
    							
    	


    }

    
 
  
}