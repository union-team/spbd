package com.spbd.cashflow.spring.model;

import com.spbd.cashflow.spring.model.Averages;

import junit.framework.TestCase;

public class AveragesTest extends TestCase {

    private Averages avg;

    protected void setUp() throws Exception {
        avg = new Averages();
    }
    
    public void testSetAndGetAvgBusIn() {
        Double testBusIn = 5.0;
        assertEquals(0, 0, 0); 
        avg.setAvgBusIn(testBusIn);
        assertEquals(testBusIn, avg.getAvgBusIn());
    }
    
    public void testSetAndGetAvgBusOut() {
    	Double testBusOut = 5.0;
    	assertEquals(0, 0, 0); 
        avg.setAvgBusOut(testBusOut);
        assertEquals(testBusOut, avg.getAvgBusOut());
    }
    
    public void testSetAndGetAvgHouseIn() {
    	Double testHouseIn = 5.0;
    	assertEquals(0, 0, 0); 
        avg.setAvgHouseIn(testHouseIn);
        assertEquals(testHouseIn, avg.getAvgHouseIn());
    }
    
    public void testSetAndGetAvgHouseOut() {
    	Double testHouseOut = 5.0;
    	assertEquals(0, 0, 0); 
        avg.setAvgHouseOut(testHouseOut);
        assertEquals(testHouseOut, avg.getAvgHouseOut());
    }
    

 
  
}