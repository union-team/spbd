package com.spbd.cashflow.spring.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import junit.framework.TestCase;

public class UserTests extends TestCase{
	
private User user;
	
	
	protected void setUp() throws Exception {
		
		user = new User();
	}

	@Test
	public void testSetAndGetId(){
		
		long testId = 1;
		assertEquals(0,0,0);
		user.setId(testId);
		assertEquals(testId,user.getId(), 0);
	}
	
	@Test
	public void testSetAndGetUsername(){
		
		String testUsername = "test";
		assertNull(user.getUsername());
		user.setUsername(testUsername);
		assertEquals(testUsername,user.getUsername());
	}
	
	@Test
	public void testSetAndGetPassword(){
		
		String testPassword = "123";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		assertEquals(testPassword,user.getPassword());
	}

	@Test
	public void testSetAndGetFirstName(){
		
		String testFirstName = "Motasem";
		assertNull(user.getFirstName());
		user.setFirstName(testFirstName);
		assertEquals(testFirstName,user.getFirstName());
	}
	
	@Test
	public void testSetAndGetLastName(){
		
		String testLastName = "Naffa";
		assertNull(user.getLastName());
		user.setLastName(testLastName);
		assertEquals(testLastName,user.getLastName());
	}
	
	@Test
	public void testSetAndGetDateOfBith() throws ParseException{
		
		DateFormat formatter ; 
		Date testDateOfBith ; 
		formatter = new SimpleDateFormat("dd-MMM-yyyy");
		testDateOfBith = formatter.parse("05-Aug-1985");
		
		assertNull(user.getDateOfBith());
		user.setDateOfBith(testDateOfBith);
		assertEquals(testDateOfBith,user.getDateOfBith());
	}
	
	@Test
	public void testSetAndGetGender(){
		
		String testGender = "Male";
		assertNull(user.getGender());
		user.setGender(testGender);
		assertEquals(testGender,user.getGender());
	}
	
	@Test
	public void testSetAndGetEmailAddress(){
		
		String testEmailAddress = "test@yahoo.com";
		assertNull(user.getEmailAddress());
		user.setEmailAddress(testEmailAddress);
		assertEquals(testEmailAddress,user.getEmailAddress());
	}
	
	@Test
	public void testSetAndGetMobileNnumber(){
		
		String testMobileNnumber = "+61 4 10855677";
		assertNull(user.getMobileNnumber());
		user.setMobileNnumber(testMobileNnumber);
		assertEquals(testMobileNnumber,user.getMobileNnumber());
	}
	
	@Test
	public void testSetAndGetCountry(){
		
		String testCountry = "Australia";
		assertNull(user.getCountry());
		user.setCountry(testCountry);
		assertEquals(testCountry,user.getCountry());
	}
	
	@Test
	public void testSetAndGetAddress(){
		
		String testAddress = "Strathfield";
		assertNull(user.getAddress());
		user.setAddress(testAddress);
		assertEquals(testAddress,user.getAddress());
	}
	
}
