<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!-- This contains the most used tag libraries -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
</head>
<body>

<br/><br />
<sec:authentication property="name"/><sec:authentication var="role" property="authorities"/>

<br />

<%
   String name = request.getUserPrincipal().getName();  
   session.setAttribute("name",name); 
   

   //String role = request.getParameter("roleName");
   //session.setAttribute("role",role);

%>

<sec:authorize access="hasRole('Admin')">
					
	<%session.setAttribute("role","Admin");%>
	
</sec:authorize>

<sec:authorize access="hasRole('User')">
					
	<%session.setAttribute("role","User");%>
	
</sec:authorize>

<c:redirect url="/index.xhtml"/>

<br>
</body>
</html>