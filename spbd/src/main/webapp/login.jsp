<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>

<style type="text/css">
	td { text-align:left; font-family: verdana,arial; color: #064073; font-size: 1.00em; }
	input { border: 1px solid #CCCCCC; border-radius: 10px; color: #666666; display: inline-block; font-size: 1.00em;  padding: 5px; width: 100%; }
	input[type="button"], input[type="reset"], input[type="submit"] { height: auto; width: auto; cursor: pointer; box-shadow: 0px 0px 5px #0361A8; float: right; margin-top: 10px; }
	table.center { margin-left:auto; margin-right:auto; }
	.error { font-family: verdana,arial; color: #D41313; font-size: 1.00em; }
	</style>

</head>
<body>
		
	<%	
   		/* session.setAttribute("name",null);
		session.setAttribute("role",null); */
	%>
	
	<div style="text-align: center;">
	<div style="box-sizing: border-box; display: inline-block; width: auto; max-width: 500px; background-color: #FFFFFF; border: 2px solid #0361A8; border-radius: 5px; box-shadow: 0px 0px 8px #0361A8; margin: 50px auto auto;">
	<div style="background: #0361A8; border-radius: 5px 5px 0px 0px; padding: 15px;"><span style="font-family: verdana,arial; color: #D4D4D4; font-size: 1.00em; font-weight:bold;">Enter your username and password</span></div>
	<div style="background: ; padding: 15px">
			
	<form id="loginForm" action="j_spring_security_check" method="POST">
		<p>Demo account: (username: demo, password: 1234)</p>
		<table class='center'>
			<tr>
				<td>Username:</td>
				<td><input type="text" name="j_username" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="j_password" /></td>
			</tr>
			<tr>
				<td colspan="2" align="right"><input type="submit" value="Login"/></td>
			</tr>
			
			<tr>
			<td colspan="2" align="left">
			
			<a href="ServiceMail.xhtml" style="font-size: 14px; text-decoration: none; font-style:italic">Forgot your Password</a>
			<br></br>
			<span style="font-size: 14px; color: red;">${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</span>
			
			</td>
			</tr>
		</table>
	</form>
	</div></div></div>
	
</body>
</html>