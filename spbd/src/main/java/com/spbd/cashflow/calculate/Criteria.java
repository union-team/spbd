package com.spbd.cashflow.calculate;

import javax.persistence.Column;

public class Criteria 
{
	// Low, High, MED Risk constants
	public static final String HIGHRISK="High";
	public static final String MEDRISK="Medium";
	public static final String LOWRISK="Low";
	public static final String NARISK="NA"; // Not Applicable
	

	// Risk criteria for output page
	private static final int[][] criteria = new int[][]{
			  { 100, 150, 150 }, //Net business income as % sales revenue (FD)
			  { 100, 150, 150 }, //Net business income as % sales revenue (Budget)
			  { 20, 50, 50 },    //Difference between present and expected income
			  { 60, 80, 80 },    //Loan repayment as % of Household Income (Financial Diary)
			  { 40, 60, 80 },    //Loan repayment as % of Household Income (Budget)
			  { 20, 50, 50 },       //Difference between present and expected income
			  { 50, 85, 85 },       //Household expenses as % of houshold income (Fin. Diary)
			  { 50, 85, 85 }        //Household expenses as % of houshold income (Budget)		  
			};
	public static final int[][] weeksToPayCriteria=new int[][]{
		{250,13},
		{350,26},
		{600,39},
		{900,52},
	};
	public static int calculateWeekstoPay(double amount)
	{
		for(int i=weeksToPayCriteria.length-1;i>0;i--)
		{
			if(amount>=weeksToPayCriteria[i][0])
			{
				return weeksToPayCriteria[i][1];
			}
		}
		return weeksToPayCriteria[0][1];
	}
	
	public static String calculateRisk(int var_no,double var_value)
	{
		if(var_value==0)
			return NARISK;
		if(var_value<criteria[var_no][0])
			return LOWRISK;
		if(var_value<=criteria[var_no][1])
			return MEDRISK;
		if(var_value<=criteria[var_no][2])
			return HIGHRISK;
		return HIGHRISK;
	}
	
	public static final String SUMMARY[]=
		{"Loan amount meets criteria, proceed with processing.",
		"Loan meets criteria, but proceed with caution. Check non-financial information.",
		"Loan meets criteria, but represents a risk at current loan amount due to: [insert indicators]. Explain risks to client and Centre Chief. Double check non-financial information.",
		"Loan represents high risk at current loan amount, due to [insert indicators]. Strongly advise client and CC to revise down loan application.",
		"Do not proceed with loan at current amount."
		};

	
}
