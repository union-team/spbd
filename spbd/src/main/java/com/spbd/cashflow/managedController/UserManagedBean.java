package com.spbd.cashflow.managedController;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.validator.ValidatorException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.expression.impl.ThisExpressionResolver;
import org.springframework.beans.factory.annotation.Autowired;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;
import com.spbd.cashflow.spring.model.Role;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.model.UserStatus;
import com.spbd.cashflow.spring.model.UsersAndRoles;
import com.spbd.cashflow.spring.service.UserService;
import com.spbd.cashflow.spring.dao.UserLoginDAO;
import com.spbd.cashflow.spring.service.UserDetailsServiceImpl;

@ManagedBean(name = "userMB")
@SessionScoped
public class UserManagedBean implements Serializable {

	private static final Logger logger = Logger.getLogger(UserManagedBean.class.getName());
	
	private static final String SUCCESS = "users";
	private static final String ERROR = "error";

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
			+ "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
			+ "(\\.[A-Za-z]{2,})$";

	@Autowired
	private UserLoginDAO UserLoginDao;

	@ManagedProperty(value = "#{UserService}")
	transient UserService userService;

	List<User> userList;
	List<User> userListGivenAccess;
	User user;
	
	// login session
	User userLogin;
	String userLoginRole;
	
	Role role;
	UserStatus userStatus;
	UsersAndRoles userANDroles;

	private long id;
	private Integer role_id;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private UserStatus status;
	private Date dateOfBith;
	private String address;
	private String country;
	private String gender;
	private String emailAddress;
	private String mobileNnumber;

	private String roleName;
	private String FName;

	public String addUser() {
		try {
			User user = new User();
			UsersAndRoles userANDroles = new UsersAndRoles();

			FacesContext ctx = FacesContext.getCurrentInstance();
			Map<String, String> request = ctx.getExternalContext()
					.getRequestParameterMap();

			user.setFirstName(getFirstName());
			user.setUsername(getUsername());
			user.setPassword(getPassword());
			user.setStatus(getStatus());
			user.setLastName(getLastName());
			user.setAddress(getAddress());
			user.setCountry(getCountry());
			user.setDateOfBith(getDateOfBith());
			user.setEmailAddress(getEmailAddress());
			user.setGender(getGender());
			user.setMobileNnumber(getMobileNnumber());

			getUserService().addUser(user);

			userANDroles.setId(user.getId());
			userANDroles.setRole_id(Integer.parseInt(request.get("myAddForm"
					+ NamingContainer.SEPARATOR_CHAR + "User_Role")));

			getUserService().addUsersAndRoles(userANDroles);
					

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ERROR;
	}
	

	// @PostConstruct
	// public void init() {
	// userList = new ArrayList<User>();
	// userList.addAll(getUserService().getUsers());
	// }

	 public void reset() throws NoSuchAlgorithmException, UnsupportedEncodingException {

	 this.setFirstName("");
	 this.setUsername("");
	 this.setPassword("");
	 this.setLastName("");
	 this.setAddress("");
	 this.setCountry("");
	 this.setDateOfBith(null);
	 this.setEmailAddress("");
	 this.setGender("");
	 this.setMobileNnumber("");

	 }

	public void delete(long id) {
		User user = userService.getUserById(id);
		if (user != null)
			userService.deleteUser(user);
	}

	public List<User> getUserList() {
		userList = new ArrayList<User>();
		userList.addAll(getUserService().getUsers());
		
		FacesContext fc = FacesContext.getCurrentInstance();   
		
		String username = (String)fc.getApplication().createValueBinding("#{name}").getValue(fc);  
				
        Iterator<User> it = userList.iterator();
        while (it.hasNext()) {
            if (it.next().getUsername().equalsIgnoreCase(username)) {
                it.remove();
            }
        }
		
		return userList;
	}
	
	public List<User> getUserListGivenAccess() {
		userList = new ArrayList<User>();
						
		if(getUserLoginRole().equalsIgnoreCase("admin")) {
			userList.addAll(getUserService().getUsers());			
		} else {
			userList.add(userLogin);
		}
		
		return userList;
	}

	public String userDetails(long id) {

		User user = new User();
		UsersAndRoles userANDroles = new UsersAndRoles();

		user = userService.getUserById(id);
		this.setUser(user);

		userANDroles = userService.getUsersAndRolesById(id);
		this.setUserANDroles(userANDroles);

		if (userANDroles.getRole_id() == 1) {
			this.setRoleName("Admin");
		}

		if (userANDroles.getRole_id() == 2) {
			this.setRoleName("Loan Officer - User");
		}

		return ("userdetails");
	}

	public void userDetailsByUsername(String username) throws IOException {

		User user = new User();
		UsersAndRoles userANDroles = new UsersAndRoles();

		user = userService.getUserByUserName(username);
		this.setUser(user);

		userANDroles = userService.getUsersAndRolesById(user.getId());
		this.setUserANDroles(userANDroles);

		if (userANDroles.getRole_id() == 1) {
			this.setRoleName("Admin");
		}

		if (userANDroles.getRole_id() == 2) {
			this.setRoleName("Loan Officer - User");
		}

		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
        String serverRealPath = servletContext.getContextPath();	    	
    	FacesContext.getCurrentInstance().getExternalContext().redirect(serverRealPath+"/userdetails.xhtml");
	}
	
	public void logout() throws IOException {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
        String serverRealPath = servletContext.getContextPath();	    	
    	FacesContext.getCurrentInstance().getExternalContext().redirect(serverRealPath+"/logout");
	}

	public String onPageLoad() {

		return "";
	}

	public String getOnPageLoad() {

		return "";
	}

	public User getUserList(long id) {

		User user = userService.getUserById(id);

		return user;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	// Attributes Getters and Setters //

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UsersAndRoles getUserANDroles() {
		return userANDroles;
	}

	public void setUserANDroles(UsersAndRoles userANDroles) {
		this.userANDroles = userANDroles;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return (password);
	}

	public void setPassword(String password) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {

		this.password = md5(password);
	}

	// convert the password to hex format md5
	private String md5(String input) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {

		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] md5hash = new byte[32];
		md.update(input.getBytes("iso-8859-1"), 0, input.length());
		md5hash = md.digest();
		return convertToHex(md5hash);
	}

	private static String convertToHex(byte[] b) {
		StringBuilder result = new StringBuilder(32);
		for (int i = 0; i < b.length; i++) {
			result.append(Integer.toString((b[i] & 0xff) + 0x100, 16)
					.substring(1));
		}

		return result.toString();
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBith() {
		return dateOfBith;
	}

	public void setDateOfBith(Date dateOfBith) {
		this.dateOfBith = dateOfBith;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobileNnumber() {
		return mobileNnumber;
	}

	public void setMobileNnumber(String mobileNnumber) {
		this.mobileNnumber = mobileNnumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getFName() {
		return FName;
	}

	public void setFName(String fName) {
		FName = fName;
	}
	
	public User getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(User userLogin) {
		this.userLogin = userLogin;
	}
	
	public String getUserLoginRole() {
		return userLoginRole;
	}
	
	public void setUserLoginRole(String userLoginRole) {
		this.userLoginRole = userLoginRole;
	}
	
	
	// End of the Getters and Setters //

	// Email Address Validation


	public void ValidateEmailAddress(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String email = (String) value;

		if (value == null) {
			return;
		}

		/* check for email address valid format */

		Boolean Valid = email.matches(EMAIL_PATTERN);

		if (!Valid) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Invalid email format.", null));
		}

		/* check email address for duplicates in the database */

		if (userService.existEmail(email) != null) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Email is already in use.",
					null));
		}
	}

	// Username duplicate Validation

	public void ValidateUsernameDuplicate(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String username = (String) value;

		if (value == null) {
			return;
		}

		/* check username for duplicates in the database */

		if (userService.getUserByUserName(username) != null) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Username is already in use.",
					null));
		}

	}

	// Password Validator

	public void PasswordValidator(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		// Cast the value of the entered password to String.
		String password = (String) value;

		// Obtain the component and submitted value of the confirm password
		// component.
		UIInput confirmComponent = (UIInput) component.getAttributes().get(
				"confirm");
		String confirm = (String) confirmComponent.getSubmittedValue();

		// Check if they both are filled in.
		if (password == null || password.isEmpty() || confirm == null
				|| confirm.isEmpty()) {
			return; // Let required="true" do its job.
		}

		// Compare the password with the confirm password.
		if (!password.equals(confirm)) {
			confirmComponent.setValid(false); // So that it's marked invalid.
			throw new ValidatorException(new FacesMessage(
					" Passwords are not equal."));
		}

		// Validate the password

		if (password.length() > 10) {
			throw new ValidatorException(
					new FacesMessage(
							" Password's Length is greater than allowable maximum of '10'."));
		}

	}

	// Mobile Number Validator

	public void MobileNumberValidator(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {

		// Cast the value of the entered mobile to String.
		String mobile = (String) value;

		String regex = "^\\+?[0-9. ()-]{10,25}$"; // "^([+]?[\d]+)?$"
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(mobile);
		if (!matcher.matches()) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Mobile Number must be in the form +61 4 10811900 .",
					null));
		}
	}
	
	// Get the User Login Details
	
	 public void preRender(ComponentSystemEvent event){
		 if(userLogin==null) {
			FacesContext fc = FacesContext.getCurrentInstance(); 
			userLogin = new User();
			
			String username = (String)fc.getApplication().createValueBinding("#{name}").getValue(fc); 
			String role = (String)fc.getApplication().createValueBinding("#{role}").getValue(fc);
			
			setUserLoginRole(role);
			
			userLogin = userService.getUserByUserName(username);
			setFName( userLogin.getFirstName() + " " + userLogin.getLastName());
			
		 }
	 }

}
