package com.spbd.cashflow.managedController.cashflow;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ibm.as400.util.commtrace.EchoReplyRequest;
import com.spbd.cashflow.managedController.SessionDataBean;

@ManagedBean(name="budgetMB")
@ViewScoped
public class BudgetManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	String SUCCESS = "review";
	String ERROR = "error";
	
	@ManagedProperty(value="#{sessionDataBean}")
    SessionDataBean sessionDataBean;
	
	double averageBusinessCashInflow;
	double averageBusinessCashOutflow;
	double averageBusinessCashNetflow;
	double averageHouseholdCashInflow;
	double averageHouseholdCashOutflow;
	double averageHouseholdCashNetflow;
	
	String businessNetColor = "color: green;";
	String householdNetColor = "color: green;";

	public String addBudget() {
		try {
			setAverageBusinessCashNetflow(getAverageBusinessCashInflow()-getAverageBusinessCashOutflow());
			setAverageHouseholdCashNetflow(getAverageHouseholdCashInflow()-getAverageHouseholdCashOutflow());
			
			sessionDataBean.getCashflowObject().getCashflow().setExpectedBusinessInAverage(getAverageBusinessCashInflow());
			sessionDataBean.getCashflowObject().getCashflow().setExpectedBusinessOutAverage(getAverageBusinessCashOutflow());
			sessionDataBean.getCashflowObject().getCashflow().setExpectedBusinessNetAverage(getAverageBusinessCashNetflow());
			sessionDataBean.getCashflowObject().getCashflow().setExpectedHouseholdInAverage(getAverageHouseholdCashInflow());
			sessionDataBean.getCashflowObject().getCashflow().setExpectedHouseholdNetAverage(getAverageHouseholdCashNetflow());
			sessionDataBean.getCashflowObject().getCashflow().setExpectedHouseholdOutAverage(getAverageHouseholdCashOutflow());
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	public SessionDataBean getSessionDataBean() {
		return sessionDataBean;
	}

	public void setSessionDataBean(SessionDataBean sessionDataBean) {
		this.sessionDataBean = sessionDataBean;
	}

	public double getAverageBusinessCashInflow() {
		return averageBusinessCashInflow;
	}

	public void setAverageBusinessCashInflow(double averageBusinessCashInflow) {
		this.averageBusinessCashInflow = averageBusinessCashInflow;
	}

	public double getAverageBusinessCashOutflow() {
		return averageBusinessCashOutflow;
	}

	public void setAverageBusinessCashOutflow(double averageBusinessCashOutflow) {
		this.averageBusinessCashOutflow = averageBusinessCashOutflow;
	}

	public double getAverageBusinessCashNetflow() {
		return averageBusinessCashNetflow;
	}

	public void setAverageBusinessCashNetflow(double averageBusinessCashNetflow) {
		this.averageBusinessCashNetflow = averageBusinessCashNetflow;
	}

	public double getAverageHouseholdCashInflow() {
		return averageHouseholdCashInflow;
	}

	public void setAverageHouseholdCashInflow(double averageHouseholdCashInflow) {
		this.averageHouseholdCashInflow = averageHouseholdCashInflow;
	}

	public double getAverageHouseholdCashOutflow() {
		return averageHouseholdCashOutflow;
	}

	public void setAverageHouseholdCashOutflow(double averageHouseholdCashOutflow) {
		this.averageHouseholdCashOutflow = averageHouseholdCashOutflow;
	}

	public double getAverageHouseholdCashNetflow() {
		return averageHouseholdCashNetflow;
	}

	public void setAverageHouseholdCashNetflow(double averageHouseholdCashNetflow) {
		this.averageHouseholdCashNetflow = averageHouseholdCashNetflow;
	}
	
	public void handleBusinessKeyEvent() {
		setAverageBusinessCashNetflow(getAverageBusinessCashInflow()-getAverageBusinessCashOutflow());
		if(getAverageBusinessCashNetflow()<0)
			businessNetColor = "color: red";
		else
			businessNetColor = "color: green";
	}
	
	public void handleHouseholdKeyEvent() {
		setAverageHouseholdCashNetflow(getAverageHouseholdCashInflow()-getAverageHouseholdCashOutflow());
		if(getAverageHouseholdCashNetflow()<0)
			householdNetColor = "color: red";
		else
			householdNetColor = "color: green";
	}

	public String getBusinessNetColor() {
		return businessNetColor;
	}

	public void setBusinessNetColor(String businessNetColor) {
		this.businessNetColor = businessNetColor;
	}

	public String getHouseholdNetColor() {
		return householdNetColor;
	}

	public void setHouseholdNetColor(String householdNetColor) {
		this.householdNetColor = householdNetColor;
	}
	
}
