package com.spbd.cashflow.managedController.cashflow;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Column;

import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;
import com.spbd.cashflow.spring.service.ApplicantService;
import com.spbd.cashflow.spring.service.CashflowService;

@ManagedBean(name = "outputMB")
@ViewScoped
public class OutputManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{sessionDataBean}")
	transient SessionDataBean sessionDataBean;

	@ManagedProperty(value = "#{CashflowService}")
	transient CashflowService cashflowService;
	
	@ManagedProperty(value = "#{ApplicantService}")
	transient ApplicantService applicantService;

	// cashflow analysis 1
	private double netBusinessIncomeAsSalesRevenueFinancialDiary;
	private String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;

	// cashflow analysis 2
	private double netBusinessIncomeAsSalesRevenueBudget;
	private String netBusinessIncomeAsSalesRevenueBudgetRisk;

	// cashflow analysis 3
	private double differenceBetweenPresentAndExpectedIncomeBusiness;
	private String differenceBetweenPresentAndExpectedIncomeBusinessRisk;

	// cashflow analysis 4
	private double loanRepaymentOfHouseholdIncomeFiancialDiary;
	private String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;

	// cashflow analysis 5
	private double loanRepaymentOfHouseholdIncomeBudget;
	private String loanRepaymentOfHouseholdIncomeBudgetRisk;

	// cashflow analysis 6
	private double differenceBetweenPresentAndExpectedHousehold;
	private String differenceBetweenPresentAndExpectedHouseholdRisk;

	// cashflow analysis 7
	private double householdExpensesOfHouseholdIncomeFinancialDiary;
	private String householdExpensesOfHouseholdIncomeFinancialDiaryRisk;

	// cashflow analysis 8
	private double householdExpensesOfHouseholdIncomeBudget;
	private String householdExpensesOfHouseholdIncomeBudgetRisk;
	
	private String summary="";

	public OutputManagedBean() {
		// netBusinessIncomeAsSalesRevenueFinancialDiary=92.9;
		// netBusinessIncomeAsSalesRevenueFinancialDiaryRisk="Medium";
	}

	@PostConstruct
	public void init() {
		if (sessionDataBean.getCashflowObject() != null) 
		{
			// perform calculations
			sessionDataBean.getCashflowObject().calculateOutput();
			// copy calculated fields in sessionDataBean
			fetchSessionData();
		}
		else
		{
			try {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Redirecting to Add Cash Flow Page."));
				FacesContext.getCurrentInstance().getExternalContext().redirect("/cashflow/loans/addcashflow.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	// accept loan and add to DB
	public String finish() {
		long id;
		sessionDataBean.getCashflowObject().getCashflow().setAccepted(true);
		try{
			id=savetoDB();
			//REST BEAN
			sessionDataBean.setCashflowObject(null);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Loan "+id+" Saved Succesfully."));
		}
		catch(Exception e)
		{
			sessionDataBean.setCashflowObject(null);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error saving Loan, Please try again."));
			return "fail";
		}
		return "finish";
	}
	
	public String rejectLoan() {
		long id;
		sessionDataBean.getCashflowObject().getCashflow().setAccepted(false);
		try{
			id=savetoDB();
			//REST BEAN
			sessionDataBean.setCashflowObject(null);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Loan "+id+" Saved Succesfully."));
		}
		catch(Exception e)
		{
			sessionDataBean.setCashflowObject(null);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error saving Loan, Please try again."));
			return "fail";
		}
		return "finish";
	}
	
	private long savetoDB()
	{
		long id = -1;
		try {
			if (sessionDataBean.getCashflowObject().getCashflow() != null) {
				// Save Cashflow to DB
				id = getCashflowService().addCashflow(sessionDataBean.getCashflowObject().getCashflow());
				getApplicantService().addApplicantCashflow(getSessionDataBean().getCashflowObject().getCashflow().getApplicant(), getSessionDataBean().getCashflowObject().getCashflow());
			}
		} catch (Exception e) {
			sessionDataBean.setCashflowObject(null);
			System.out.println("ERROR saving Cashflow to DB");
		}
		return id;
	}

	// Copy session data
	private void fetchSessionData() {
		netBusinessIncomeAsSalesRevenueFinancialDiary = sessionDataBean
				.getCashflowObject().getCashflow()
				.getNetBusinessIncomeAsSalesRevenueFinancialDiary();
		
		netBusinessIncomeAsSalesRevenueFinancialDiaryRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk();
		// netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;

		// cashflow analysis 2
		netBusinessIncomeAsSalesRevenueBudget = sessionDataBean
				.getCashflowObject().getCashflow()
				.getNetBusinessIncomeAsSalesRevenueBudget();
		netBusinessIncomeAsSalesRevenueBudgetRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getNetBusinessIncomeAsSalesRevenueBudgetRisk();

		// cashflow analysis 3
		differenceBetweenPresentAndExpectedIncomeBusiness = sessionDataBean
				.getCashflowObject().getCashflow()
				.getDifferenceBetweenPresentAndExpectedIncomeBusiness();
		differenceBetweenPresentAndExpectedIncomeBusinessRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk();

		// cashflow analysis 4
		loanRepaymentOfHouseholdIncomeFiancialDiary = sessionDataBean
				.getCashflowObject().getCashflow()
				.getLoanRepaymentOfHouseholdIncomeFiancialDiary();
		loanRepaymentOfHouseholdIncomeFiancialDiaryRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk();
		//
		// cashflow analysis 5
		loanRepaymentOfHouseholdIncomeBudget = sessionDataBean
				.getCashflowObject().getCashflow()
				.getLoanRepaymentOfHouseholdIncomeBudget();
		loanRepaymentOfHouseholdIncomeBudgetRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getLoanRepaymentOfHouseholdIncomeBudgetRisk();
		// private String loanRepaymentOfHouseholdIncomeBudgetRisk;

		// cashflow analysis 6
		differenceBetweenPresentAndExpectedHousehold = sessionDataBean
				.getCashflowObject().getCashflow()
				.getDifferenceBetweenPresentAndExpectedHousehold();
		differenceBetweenPresentAndExpectedHouseholdRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getDifferenceBetweenPresentAndExpectedHouseholdRisk();
		// private String differenceBetweenPresentAndExpectedHouseholdRisk;

		// cashflow analysis 7
		householdExpensesOfHouseholdIncomeFinancialDiary = sessionDataBean
				.getCashflowObject().getCashflow()
				.getHouseholdExpensesOfHouseholdIncomeFinancialDiary();
		householdExpensesOfHouseholdIncomeFinancialDiaryRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk();
		// private String householdExpensesOfHouseholdIncomeFinancialDiaryRisk;

		// cashflow analysis 8
		householdExpensesOfHouseholdIncomeBudget = sessionDataBean
				.getCashflowObject().getCashflow()
				.getHouseholdExpensesOfHouseholdIncomeBudget();
		householdExpensesOfHouseholdIncomeBudgetRisk = sessionDataBean
				.getCashflowObject().getCashflow()
				.getHouseholdExpensesOfHouseholdIncomeBudgetRisk();
		// private String householdExpensesOfHouseholdIncomeBudgetRisk;
		
		summary=sessionDataBean.getCashflowObject().getCashflow().getSummary();
	}

	// Enter calculation Data
	// 8abe16fd4883f3d0014883f4c3600001

	// GETTER & SETTERS
	public SessionDataBean getSessionDataBean() {
		return sessionDataBean;
	}

	public void setSessionDataBean(SessionDataBean sessionDataBean) {
		this.sessionDataBean = sessionDataBean;
	}

	public CashflowService getCashflowService() {
		return cashflowService;
	}

	public void setCashflowService(CashflowService cashflowService) {
		this.cashflowService = cashflowService;
	}

	public double getNetBusinessIncomeAsSalesRevenueFinancialDiary() {
		return netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiary(
			double netBusinessIncomeAsSalesRevenueFinancialDiary) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiary = netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public String getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk() {
		return netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(
			String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiaryRisk = netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public double getNetBusinessIncomeAsSalesRevenueBudget() {
		return netBusinessIncomeAsSalesRevenueBudget;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudget(
			double netBusinessIncomeAsSalesRevenueBudget) {
		this.netBusinessIncomeAsSalesRevenueBudget = netBusinessIncomeAsSalesRevenueBudget;
	}

	public String getNetBusinessIncomeAsSalesRevenueBudgetRisk() {
		return netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudgetRisk(
			String netBusinessIncomeAsSalesRevenueBudgetRisk) {
		this.netBusinessIncomeAsSalesRevenueBudgetRisk = netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public double getDifferenceBetweenPresentAndExpectedIncomeBusiness() {
		return differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusiness(
			double differenceBetweenPresentAndExpectedIncomeBusiness) {
		this.differenceBetweenPresentAndExpectedIncomeBusiness = differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public String getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk() {
		return differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(
			String differenceBetweenPresentAndExpectedIncomeBusinessRisk) {
		this.differenceBetweenPresentAndExpectedIncomeBusinessRisk = differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public double getLoanRepaymentOfHouseholdIncomeFiancialDiary() {
		return loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiary(
			double loanRepaymentOfHouseholdIncomeFiancialDiary) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiary = loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public String getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk() {
		return loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(
			String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiaryRisk = loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public double getLoanRepaymentOfHouseholdIncomeBudget() {
		return loanRepaymentOfHouseholdIncomeBudget;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudget(
			double loanRepaymentOfHouseholdIncomeBudget) {
		this.loanRepaymentOfHouseholdIncomeBudget = loanRepaymentOfHouseholdIncomeBudget;
	}

	public String getLoanRepaymentOfHouseholdIncomeBudgetRisk() {
		return loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudgetRisk(
			String loanRepaymentOfHouseholdIncomeBudgetRisk) {
		this.loanRepaymentOfHouseholdIncomeBudgetRisk = loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public double getDifferenceBetweenPresentAndExpectedHousehold() {
		return differenceBetweenPresentAndExpectedHousehold;
	}

	public void setDifferenceBetweenPresentAndExpectedHousehold(
			double differenceBetweenPresentAndExpectedHousehold) {
		this.differenceBetweenPresentAndExpectedHousehold = differenceBetweenPresentAndExpectedHousehold;
	}

	public String getDifferenceBetweenPresentAndExpectedHouseholdRisk() {
		return differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedHouseholdRisk(
			String differenceBetweenPresentAndExpectedHouseholdRisk) {
		this.differenceBetweenPresentAndExpectedHouseholdRisk = differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public double getHouseholdExpensesOfHouseholdIncomeFinancialDiary() {
		return householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiary(
			double householdExpensesOfHouseholdIncomeFinancialDiary) {
		this.householdExpensesOfHouseholdIncomeFinancialDiary = householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public String getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk() {
		return householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(
			String householdExpensesOfHouseholdIncomeFinancialDiaryRisk) {
		this.householdExpensesOfHouseholdIncomeFinancialDiaryRisk = householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public double getHouseholdExpensesOfHouseholdIncomeBudget() {
		return householdExpensesOfHouseholdIncomeBudget;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudget(
			double householdExpensesOfHouseholdIncomeBudget) {
		this.householdExpensesOfHouseholdIncomeBudget = householdExpensesOfHouseholdIncomeBudget;
	}

	public String getHouseholdExpensesOfHouseholdIncomeBudgetRisk() {
		return householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudgetRisk(
			String householdExpensesOfHouseholdIncomeBudgetRisk) {
		this.householdExpensesOfHouseholdIncomeBudgetRisk = householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public ApplicantService getApplicantService() {
		return applicantService;
	}

	public void setApplicantService(ApplicantService applicantService) {
		this.applicantService = applicantService;
	}
	
	

}
