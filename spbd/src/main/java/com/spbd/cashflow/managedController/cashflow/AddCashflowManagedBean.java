package com.spbd.cashflow.managedController.cashflow;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.service.CashflowService;

@ManagedBean(name="addCashflowMB")
@RequestScoped
public class AddCashflowManagedBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    String BUDGET = "budget";
    String DIARY = "diary";
    String ERROR = "error";
    
    @ManagedProperty(value="#{sessionDataBean}")
    SessionDataBean sessionDataBean;
    
    @ManagedProperty(value="#{CashflowService}")
    CashflowService cashflowService;

    Applicant applicant;
    private double loanAmount;
    Date dateSubmitted;
    
    ArrayList<Cashflow> cashflows;
 
    public String addCashflow() {
        try {
        	sessionDataBean.init();
        	sessionDataBean.getCashflowObject().getCashflow().setLoanAmount(getLoanAmount());;
        	sessionDataBean.getCashflowObject().getCashflow().setApplicant(getApplicant());
        	sessionDataBean.getCashflowObject().getCashflow().setDateSubmitted(getDateSubmitted());
        	if(getLoanAmount()>1500) {
        		return DIARY;
        	}
            return BUDGET;
        } catch (Exception e) {
            e.printStackTrace();
        }   
 
        return ERROR;
    }

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public SessionDataBean getSessionDataBean() {
		return sessionDataBean;
	}

	public void setSessionDataBean(SessionDataBean sessionDataBean) {
		this.sessionDataBean = sessionDataBean;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public Date getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public ArrayList<Cashflow> getCashflows() {
		cashflows = new ArrayList<Cashflow>();
		cashflows.addAll(getCashflowService().getCashflows());
		return cashflows;
	}

	public void setCashflows(ArrayList<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

	public CashflowService getCashflowService() {
		return cashflowService;
	}

	public void setCashflowService(CashflowService cashflowService) {
		this.cashflowService = cashflowService;
	}
    
}