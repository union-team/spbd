package com.spbd.cashflow.managedController;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.spbd.cashflow.calculate.CashflowAnalysis;

// common session bean used by everyone
@ManagedBean(name="sessionDataBean")
@SessionScoped
public class SessionDataBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	// This object will store all data from cashflow 
	CashflowAnalysis cashflowObject;
	
	public SessionDataBean()
	{
		//for test purposes only 
	//	init();
	}
	
	public void init() {
		cashflowObject = new CashflowAnalysis();
	}
	
	public CashflowAnalysis getCashflowObject() {
		return cashflowObject;
	}

	public void setCashflowObject(CashflowAnalysis cashflowObject) {
		this.cashflowObject = cashflowObject;
	}
	
}
