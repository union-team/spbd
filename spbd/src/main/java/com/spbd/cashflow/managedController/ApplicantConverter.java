package com.spbd.cashflow.managedController;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ApplicantService;
 
 
@ManagedBean
@RequestScoped
public class ApplicantConverter implements Converter {
	
	@ManagedProperty(value="#{ApplicantService}")
    ApplicantService service;
 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            return service.getApplicantById(Long.valueOf(value));
        }
        else {
            return null;
        }
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
        	return String.valueOf(((Applicant) object).getId());
        }
        else {
            return null;
        }
    }

	public ApplicantService getService() {
		return service;
	}

	public void setService(ApplicantService service) {
		this.service = service;
	}
}    