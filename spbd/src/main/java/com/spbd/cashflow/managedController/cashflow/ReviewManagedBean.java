package com.spbd.cashflow.managedController.cashflow;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PrePersist;

import com.spbd.cashflow.calculate.CashflowAnalysis;
import com.spbd.cashflow.managedController.Guideline;
import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Cashflow;
import com.sun.faces.facelets.tag.jstl.core.SetHandler;

@ManagedBean(name="reviewMB")
@ViewScoped
public class ReviewManagedBean implements Serializable {
	private static final long serialVersionUID = 1L;
	String OUPUT = "output";
	String STARTOVER = "addcashflow";
	String ERROR = "error";
	
	@ManagedProperty(value="#{sessionDataBean}")
    SessionDataBean sessionDataBean;
	
	double loanAmount;
	
	double pastBusinessIn;
	double pastBusinessOut;
	double pastBusinessNet;
	
	double pastHouseholdIn;
	double pastHouseholdOut;
	double pastHouseholdNet;
	
	double budgetBusinessIn;
	double budgetBusinessOut;
	double budgetBusinessNet;
	
	double budgetHouseholdIn;
	double budgetHouseholdOut;
	double budgetHouseholdNet;
	
	ArrayList<Guideline> guidelines;
	
	@PostConstruct
	public void populate() {
		Cashflow cf = getSessionDataBean().getCashflowObject().getCashflow();
		
		setLoanAmount(cf.getLoanAmount());
		
		setPastBusinessIn(cf.getDiaryBusinessInAverage());
		setPastBusinessOut(cf.getDiaryBusinessOutAverage());
		setPastBusinessNet(cf.getDiaryBusinessNetAverage());
		
		setPastHouseholdIn(cf.getDiaryHouseholdInAverage());
		setPastHouseholdOut(cf.getDiaryHouseholdOutAverage());
		setPastHouseholdNet(cf.getDiaryHouseholdNetAverage());
		
		setBudgetBusinessIn(cf.getExpectedBusinessInAverage());
		setBudgetBusinessOut(cf.getExpectedBusinessOutAverage());
		setBudgetBusinessNet(cf.getExpectedBusinessNetAverage());
		
		setBudgetHouseholdIn(cf.getExpectedHouseholdInAverage());
		setBudgetHouseholdOut(cf.getExpectedHouseholdOutAverage());
		setBudgetHouseholdNet(cf.getExpectedHouseholdNetAverage());
		
		populateGuidelines();
		
	}


	public SessionDataBean getSessionDataBean() {
		return sessionDataBean;
	}

	public void setSessionDataBean(SessionDataBean sessionDataBean) {
		this.sessionDataBean = sessionDataBean;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public double getPastBusinessIn() {
		return pastBusinessIn;
	}

	public void setPastBusinessIn(double pastBusinessIn) {
		this.pastBusinessIn = pastBusinessIn;
	}

	public double getPastBusinessOut() {
		return pastBusinessOut;
	}

	public void setPastBusinessOut(double pastBusinessOut) {
		this.pastBusinessOut = pastBusinessOut;
	}

	public double getPastBusinessNet() {
		return pastBusinessNet;
	}

	public void setPastBusinessNet(double pastBusinessNet) {
		this.pastBusinessNet = pastBusinessNet;
	}

	public double getPastHouseholdIn() {
		return pastHouseholdIn;
	}

	public void setPastHouseholdIn(double pastHouseholdIn) {
		this.pastHouseholdIn = pastHouseholdIn;
	}

	public double getPastHouseholdOut() {
		return pastHouseholdOut;
	}

	public void setPastHouseholdOut(double pastHouseholdOut) {
		this.pastHouseholdOut = pastHouseholdOut;
	}

	public double getPastHouseholdNet() {
		return pastHouseholdNet;
	}

	public void setPastHouseholdNet(double pastHouseholdNet) {
		this.pastHouseholdNet = pastHouseholdNet;
	}

	public double getBudgetBusinessIn() {
		return budgetBusinessIn;
	}

	public void setBudgetBusinessIn(double budgetBusinessIn) {
		this.budgetBusinessIn = budgetBusinessIn;
	}

	public double getBudgetBusinessOut() {
		return budgetBusinessOut;
	}

	public void setBudgetBusinessOut(double budgetBusinessOut) {
		this.budgetBusinessOut = budgetBusinessOut;
	}

	public double getBudgetBusinessNet() {
		return budgetBusinessNet;
	}

	public void setBudgetBusinessNet(double budgetBusinessNet) {
		this.budgetBusinessNet = budgetBusinessNet;
	}

	public double getBudgetHouseholdIn() {
		return budgetHouseholdIn;
	}

	public void setBudgetHouseholdIn(double budgetHouseholdIn) {
		this.budgetHouseholdIn = budgetHouseholdIn;
	}

	public double getBudgetHouseholdOut() {
		return budgetHouseholdOut;
	}

	public void setBudgetHouseholdOut(double budgetHouseholdOut) {
		this.budgetHouseholdOut = budgetHouseholdOut;
	}

	public double getBudgetHouseholdNet() {
		return budgetHouseholdNet;
	}

	public void setBudgetHouseholdNet(double budgetHouseholdNet) {
		this.budgetHouseholdNet = budgetHouseholdNet;
	}

	public ArrayList<Guideline> getGuidelines() {
		return guidelines;
	}

	public void setGuidelines(ArrayList<Guideline> guidelines) {
		this.guidelines = guidelines;
	}
	
	public void populateGuidelines() {
		guidelines = new ArrayList<Guideline>();
		guidelines.add(new Guideline("BBQ",0,380));
		guidelines.add(new Guideline("Bakery",40,180));
		guidelines.add(new Guideline("Beauty/hairdresser",0,0));
		guidelines.add(new Guideline("Canteen",30,80));
		guidelines.add(new Guideline("Fish selling",15,130));
		guidelines.add(new Guideline("Florist",77,450));
		guidelines.add(new Guideline("Food service",20,125));
		guidelines.add(new Guideline("Freshwater Mussels (Kai)",55,185));
		guidelines.add(new Guideline("Frozen Food - Ice Cream",20,70));
		guidelines.add(new Guideline("Frozen Food - Meat",30,40));
		guidelines.add(new Guideline("Fuel Kerosene",10,10));
		guidelines.add(new Guideline("Handicrafts",80,330));
		guidelines.add(new Guideline("Kava",0,70));
		guidelines.add(new Guideline("Lawn Care (Brush cutter)",10,38));
		guidelines.add(new Guideline("Livestock - Poultry/Pigs",0,0));
		guidelines.add(new Guideline("MPAISA",0,165));
		guidelines.add(new Guideline("Screen Painting",0,0));
		guidelines.add(new Guideline("Sewing",30,130));
		guidelines.add(new Guideline("Market Vendor",130,900));
		guidelines.add(new Guideline("Weaving",0,0));
	}
	

}
