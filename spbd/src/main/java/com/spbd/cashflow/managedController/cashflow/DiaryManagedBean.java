package com.spbd.cashflow.managedController.cashflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.CellEditEvent;

import com.spbd.cashflow.calculate.CashflowAnalysis;
import com.spbd.cashflow.managedController.SessionDataBean;
import com.spbd.cashflow.spring.model.Averages;
import com.spbd.cashflow.spring.model.Diary;

@ManagedBean(name="diaryMB")
@ViewScoped
public class DiaryManagedBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final String SUCCESS = "next";
    private static final String ERROR   = "error";
	
	@ManagedProperty(value="#{sessionDataBean}")
    SessionDataBean sessionDataBean;
	
	//variables for diaries
	List<Diary> diaries;
	
	String id;
	int monthNumber;
	double businessIn;
	double businessOut;
	double householdIn;
	double householdOut;
	
	//spinner and visibility variables
	private int number1 = 1; 
    private boolean show;
    
    //variables for averages
    List<Averages> avgs;
	
	double avgBusIn;
    double avgBusOut;
    double avgHouseIn;
    double avgHouseOut;
    
    private boolean showAvg;
    

	@PostConstruct
    public void init() {
        diaries = createDiaries(1);
        avgs = createAvgs();
    }
	
	//initialise the list of diaries
    public List<Diary> createDiaries(int size) {
    	List<Diary> list = new ArrayList<Diary>();
    	for(int i = 0; i < size; i++){
    		Diary temp = new Diary();
    		temp.setMonthNumber(i+1);
    		temp.setBusinessIn(0);
    		temp.setBusinessOut(0);
    		temp.setHouseholdIn(0);
    		temp.setHouseholdOut(0);
    		list.add(temp);
    	}
    	return list;
    }
    
    //initialise averages list for datatable
    public List<Averages> createAvgs() {
    	List<Averages> list = new ArrayList<Averages>();
    	Averages avg = new Averages();
    	avg.setAvgBusIn(0);
    	avg.setAvgBusOut(0);
    	avg.setAvgHouseIn(0);
    	avg.setAvgHouseOut(0);
    	list.add(avg);
    	return list;
    }
    
    
	//function to add diaries to current cashflow object when next is pressed
	public String addDiary() {
		try {
			sessionDataBean.getCashflowObject().getCashflow().setDiaries(diaries);	
			sessionDataBean.getCashflowObject().diaryCalculations();
			
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	public void add() {
		try {
			sessionDataBean.getCashflowObject().getCashflow().setDiaries(diaries);	
			sessionDataBean.getCashflowObject().diaryCalculations();
			
		} catch (Exception e) {
		}
	}
	
	
	public List<Diary> getDiaries() {
		return diaries;
	}
	
	public void setDiaries(List<Diary> diaries) {
		this.diaries = diaries;
	}
    
    public int getMonthNumber() {
    	return monthNumber;
    }
    
    public void setMonthNumber(int monthNumber) {
    	this.monthNumber = monthNumber;
    }
    
    public double getbusinessIn() {
    	return businessIn;
    }
    
    public void setBusinessIn(double businessIn) {
    	this.businessIn = businessIn;
    }
    
    public double getbusinessOut() {
    	return businessIn;
    }
    
    public void setBusinessOut(double businessOut) {
    	this.businessOut = businessOut;
    }
    
    public double getHouseholdIn() {
    	return householdIn;
    }
    
    public void setHouseholdIn(double householdIn) {
    	this.householdIn = householdIn;
    }
    
    public double getHouseholdOut() {
    	return householdIn;
    }
    
    public void setHouseholdOut(double householdOut) {
    	this.householdOut = householdOut;
    }
    
    
    public int getNumber1() {
        return number1;
    }
 
    public void setNumber1(int number1) {
        this.number1 = number1;
    }
 
   public boolean getShow() {
	   return show;
   }
   
   public void setShow(boolean show) {
	   this.show = show;
   }
   
   
   
   //to show diaries datatable after spinner is populated
   public void setShowTrue(ActionEvent e) {
	   setShow(true);
	   if(number1 < diaries.size())
	   {
		   int size = diaries.size();
		   for(int i = number1; i < size; i++){
			   diaries.remove(number1);
		   }
	   } else if (number1 > diaries.size()) {
		   for(int i = diaries.size()+1; i <= number1; i++){
			   Diary temp = new Diary();
			   temp.setMonthNumber(i);
			   temp.setBusinessIn(0);
			   temp.setBusinessOut(0);
			   temp.setHouseholdIn(0);
			   temp.setHouseholdOut(0);
			   diaries.add(temp);
		   }
	   }
   }
   
   
   public List<Averages> getAvgs(){
		return avgs;
	}
	
	public void setAvgs(List<Averages> avgs){
		this.avgs = avgs;
	}
	
	public double getAvgBusIn() {
	 	   return avgBusIn;
	    }
	    
	    public void setAvgBusIn(double avgBusIn) {
	 	   this.avgBusIn = avgBusIn;
	    }
	    
	    public double getAvgBusOut() {
	 	   return avgBusOut;
	    }
	    
	    public void setAvgBusOut(double avgBusOut) {
	 	   this.avgBusOut = avgBusOut;
	    }
	    
	    public double getAvgHouseIn() {
	 	   return avgHouseIn;
	    }
	    
	    public void setAvgHouseIn(double avgHouseIn) {
	 	   this.avgHouseIn = avgHouseIn;
	    }
	    
	    public double getAvgHouseOut() {
	 	   return avgHouseOut;
	    }
	    
	    public void setAvgHouseOut(double avgHouseOut) {
	 	   this.avgHouseOut = avgHouseOut;
	    }
	    
		
		public boolean getShowAvg() {
			return showAvg;
		}
		   
		public void setShowAvg(boolean showAvg) {
			this.showAvg = showAvg;
	    }
	    
		//show averages datatable when button is clicked
	    public void showAvgs() {
	 	   setShowAvg(true);
	 	   Averages temp = new Averages();
	 	  try {
				sessionDataBean.getCashflowObject().getCashflow().setDiaries(diaries);
				sessionDataBean.getCashflowObject().diaryCalculations();
				temp.setAvgBusIn(sessionDataBean.getCashflowObject().getCashflow().getDiaryBusinessInAverage());
				temp.setAvgBusOut(sessionDataBean.getCashflowObject().getCashflow().getDiaryBusinessOutAverage());
				temp.setAvgHouseIn(sessionDataBean.getCashflowObject().getCashflow().getDiaryHouseholdInAverage());
				temp.setAvgHouseOut(sessionDataBean.getCashflowObject().getCashflow().getDiaryHouseholdOutAverage());
				avgs.set(0, temp);
			} catch (Exception e) {
			}
	    }

	public SessionDataBean getSessionDataBean() {
		return sessionDataBean;
	}

	public void setSessionDataBean(SessionDataBean sessionDataBean) {
		this.sessionDataBean = sessionDataBean;
	}
	
	//called when a cell is edited to display message
	public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        //messages
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
	
	
	

}