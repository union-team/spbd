package com.spbd.cashflow.managedController;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.ServletContext;

import com.spbd.cashflow.spring.model.Role;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.model.UserStatus;
import com.spbd.cashflow.spring.model.UsersAndRoles;
import com.spbd.cashflow.spring.service.UserService;
import com.sun.el.parser.ParseException;

@ManagedBean(name="userUpdateMB")
@SessionScoped
public class UserUpdateManagedBean implements Serializable {

	    private static final String SUCCESS = "users";
	    private static final String ERROR   = "error";
	    
	    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
				+ "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
				+ "(\\.[A-Za-z]{2,})$";
	    
	    private Date dateOfBirth;
	 
	    @ManagedProperty(value="#{UserService}")
	    transient UserService userService;
	 
	    List<User> userList;
	    User user;
	    Role role;
	    UserStatus userStatus;
	    UsersAndRoles userANDroles;
	    	    
	    private long id;
	    private Integer role_id;
	    private String firstName;
	    private String lastName;
	    private String username;
	    private String password;
		private UserStatus status;
	    private Date dateOfBith;
	    private String address;
	    private String country;
	    private String gender;
	    private String emailAddress;
	    private String mobileNnumber;
	    
	 // Update User Details From Users Page
	    
		public String updateUser(long id) {
    	   try {
           	
    		FacesContext ctx = FacesContext.getCurrentInstance(); 
    		Map<String,String> request = ctx.getExternalContext().getRequestParameterMap();
    		   
    		User user = new User();
    		UsersAndRoles userANDroles = new UsersAndRoles();

           	user = userService.getUserById(id);
   	
           	userANDroles = userService.getUsersAndRolesById(id);           	
           	    		
           	user.setFirstName(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"firstName"));
           	user.setLastName(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"lastName"));
           	user.setUsername(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"username"));
           	//user.setPassword(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"password")); 
           	
           	String User_Status = request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"status").toString();
           	if(User_Status.equalsIgnoreCase("ACTIVE"))
           	{
           		user.setStatus(UserStatus.ACTIVE);
           	}
           	if(User_Status.equalsIgnoreCase("INACTIVE"))
           	{
           		user.setStatus(UserStatus.INACTIVE);
           	}           
           	user.setAddress(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"address"));
           	user.setCountry(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"countrybox"));
           	user.setDateOfBith(dateOfBirth);
           	user.setEmailAddress(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"email"));
           	user.setGender(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"genderbox"));
           	user.setMobileNnumber(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"mobileNnumber"));
           	
           	this.setUser(user);

          	getUserService().updateUser(user);
          	
          	getUserService().deleteUsersAndRoles(userANDroles);
          	
          	userANDroles.setId(user.getId());
          	userANDroles.setRole_id(Integer.parseInt(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"roleid")));
          	
			getUserService().addUsersAndRoles(userANDroles);
           	
            return SUCCESS;
           } catch (Exception e) {
               e.printStackTrace();
           }   
    
           return ERROR;               
	    }
		
		
		// Update User Details From Settings
		
		public String updateUserBySettings(long id) {
	    	   try {
	           	
	    		FacesContext ctx = FacesContext.getCurrentInstance(); 
	    		Map<String,String> request = ctx.getExternalContext().getRequestParameterMap();
	    		   
	    		User user = new User();
	    		UsersAndRoles userANDroles = new UsersAndRoles();

	           	user = userService.getUserById(id);
	   	
	           	userANDroles = userService.getUsersAndRolesById(id);           	
	           	    		
	           	user.setFirstName(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"firstName"));
	           	user.setLastName(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"lastName"));
	           	user.setUsername(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"username"));
	           	user.setPassword(md5(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"password"))); 
	           	     
	           	user.setAddress(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"address"));
	           	user.setCountry(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"countrybox"));
	           	user.setDateOfBith(dateOfBirth);
	           	user.setEmailAddress(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"email"));
	           	user.setGender(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"genderbox"));
	           	user.setMobileNnumber(request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"mobileNnumber"));
	           	
	           	this.setUser(user);

	          	getUserService().updateUser(user);
	          		           	
	            return "index";
	           } catch (Exception e) {
	               e.printStackTrace();
	           }   
	    
	           return ERROR;               
		    }
		
	        
	    public String userUpdate(long id) {
	    	
	    	User user = new User();

	    	user = userService.getUserById(id);
	    	this.setUser(user);
	    	this.setDateOfBith(user.getDateOfBith());
	    	
	    	userANDroles = userService.getUsersAndRolesById(id);
           	this.setUserANDroles(userANDroles);
	    	
	    	return ("userupdate");
	    }
	    
	    public void userUpdateByUsername(String username) throws IOException {
	    	User user = new User();

	    	user = userService.getUserByUserName(username);
	    	this.setUser(user);
	    	this.setDateOfBith(user.getDateOfBith());
	    		    	    	
	        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
	        String serverRealPath = servletContext.getContextPath();	    	
	    	FacesContext.getCurrentInstance().getExternalContext().redirect(serverRealPath+"/settings.xhtml");
	 	}

		public UserService getUserService() {
			return userService;
		}

		public void setUserService(UserService userService) {
			this.userService = userService;
		}

		// Attributes Getters and Setters //
		
		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
		
		public long getId() {
			return id;
		}


		public void setId(long id) {
			this.id = id;
		}


		public Integer getRole_id() {
			return role_id;
		}

		public void setRole_id(Integer role_id) {
			this.role_id = role_id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) throws NoSuchAlgorithmException,
		UnsupportedEncodingException {
			
			this.password = md5(password);
		}
		
		// convert the password to hex format md5
		private String md5(String input) throws NoSuchAlgorithmException,
				UnsupportedEncodingException {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] md5hash = new byte[32];
			md.update(input.getBytes("iso-8859-1"), 0, input.length());
			md5hash = md.digest();
			return convertToHex(md5hash);
		}

		private static String convertToHex(byte[] b) {
			StringBuilder result = new StringBuilder(32);
			for (int i = 0; i < b.length; i++) {
				result.append(Integer.toString((b[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			return result.toString();
		}

		public UserStatus getStatus() {
			return status;
		}

		public void setStatus(UserStatus status) {
			this.status = status;
		}

		public UserStatus getUserStatus() {
			return userStatus;
		}

		public void setUserStatus(UserStatus userStatus) {
			this.userStatus = userStatus;
		}

		public void setUserList(List<User> userList) {
			this.userList = userList;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public Date getDateOfBith() {
			return dateOfBith;
		}

		public void setDateOfBith(Date dateOfBith) {
			this.dateOfBith = dateOfBith;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getEmailAddress() {
			return emailAddress;
		}

		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}

		public String getMobileNnumber() {
			return mobileNnumber;
		}

		public void setMobileNnumber(String mobileNnumber) {
			this.mobileNnumber = mobileNnumber;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public UsersAndRoles getUserANDroles() {
			return userANDroles;
		}

		public void setUserANDroles(UsersAndRoles userANDroles) {
			this.userANDroles = userANDroles;
		}
	    
		// End of the Getters and Setters //
		
		// Email Address Validation

		public void ValidateEmailAddress(FacesContext context,
				UIComponent component, Object value) throws ValidatorException {
			
			FacesContext ctx = FacesContext.getCurrentInstance(); 
    		Map<String,String> request = ctx.getExternalContext().getRequestParameterMap();
    		
			String email = (String) value;
			String idStr = request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"hiddenId");
			long id = Long.valueOf(idStr);
			
			if (value == null) {
				return;
			}

			/* check for email address valid format */

			Boolean Valid = email.matches(EMAIL_PATTERN);

			if (!Valid) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Invalid email format.", null));
			}

			/* check email address for duplicates in the database */

			if (userService.existUpdateEmail(id,email) != null) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Email is already in use.",
						null));
			}
		}

		// Username duplicate Validation

		public void ValidateUsernameDuplicate(FacesContext context,
				UIComponent component, Object value) throws ValidatorException {
			
			FacesContext ctx = FacesContext.getCurrentInstance(); 
    		Map<String,String> request = ctx.getExternalContext().getRequestParameterMap();
			
			String username = (String) value;
			String idStr = request.get("myForm"+NamingContainer.SEPARATOR_CHAR+"hiddenId");
			long id = Long.valueOf(idStr);

			if (value == null) {
				return;
			}

			/* check username for duplicates in the database */

			if (userService.getUpdateUserByUserName(id, username) != null) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Username is already in use.",
						null));
			}

		}

		// Password Validator

		public void PasswordValidator(FacesContext context, UIComponent component,
				Object value) throws ValidatorException {

			// Cast the value of the entered password to String.
			String password = (String) value;

			// Obtain the component and submitted value of the confirm password
			// component.
			UIInput confirmComponent = (UIInput) component.getAttributes().get(
					"confirm");
			String confirm = (String) confirmComponent.getSubmittedValue();

			// Check if they both are filled in.
			if (password == null || password.isEmpty() || confirm == null
					|| confirm.isEmpty()) {
				return; // Let required="true" do its job.
			}

			// Compare the password with the confirm password.
			if (!password.equals(confirm)) {
				confirmComponent.setValid(false); // So that it's marked invalid.
				throw new ValidatorException(new FacesMessage(
						" Passwords are not equal."));
			}

			// You can even validate the minimum password length here and throw
			// accordingly.
			// Or, if you're smart, calculate the password strength and throw
			// accordingly ;)

			if (password.length() > 10) {
				throw new ValidatorException(
						new FacesMessage(
								" Password's Length is greater than allowable maximum of '10'."));
			}

		}

		// Mobile Number Validator

		public void MobileNumberValidator(FacesContext context,
				UIComponent component, Object value) throws ValidatorException {

			// Cast the value of the entered mobile to String.
			String mobile = (String) value;

			String regex = "^\\+?[0-9. ()-]{10,25}$";    //  "^([+]?[\d]+)?$"
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(mobile);
			if (!matcher.matches()) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Mobile Number must be in the form +61 4 10811900 .",
						null));
			}
		}
		
		// DOB Validator
		
		public void DOBValidator(FacesContext context,
				UIComponent component, Object value) throws ValidatorException, ParseException {
		
			// Cast the value of the entered DOB to String.
			Date DOB = (Date) value;
																
			if (DOB != null) {						
				dateOfBirth = DOB;
			}
		}
	}
