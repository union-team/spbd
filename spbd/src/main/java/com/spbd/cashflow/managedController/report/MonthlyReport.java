package com.spbd.cashflow.managedController.report;

import java.util.Date;

	public class MonthlyReport {
		Date startDate;
		Date endDate;
		float loanAmount=0;
		public Date getStartDate() {
			return startDate;
		}
		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}
		public Date getEndDate() {
			return endDate;
		}
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		public float getLoanAmount() {
			return loanAmount;
		}
		public void setLoanAmount(float loanAmount) {
			this.loanAmount = loanAmount;
		}
		public void addLoanAmount(double d) {
			this.loanAmount +=d;
		}
		public MonthlyReport() {
			
		}
	    public static Date subtractMonth(Date d) {
	    	Date d0 = new Date();
	    	d0.setDate(d.getDate());
	    	if (d.getMonth()>0) {
	    		d0.setMonth(d.getMonth()-1);
	    		d0.setYear(d.getYear());
	    	}
	    	else if (d.getMonth()==0) {
	    		d0.setMonth(11);
	    		d0.setYear(d.getYear()-1);
	    	}
	    	return d0;
	    }
	}