package com.spbd.cashflow.managedController.report;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ReportService;
import com.spbd.cashflow.spring.service.UserService;
 
 
@ManagedBean
@RequestScoped
public class ReportConverter implements Converter {
	
	@ManagedProperty(value="#{ReportService}")
    ReportService service;
 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            return service.getUserById(Long.valueOf(value));
        }
        else {
            return null;
        }
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
        	return String.valueOf(((User) object).getId());
        }
        else {
            return null;
        }
    }

	public ReportService getService() {
		return service;
	}

	public void setService(ReportService service) {
		this.service = service;
	}   
}    