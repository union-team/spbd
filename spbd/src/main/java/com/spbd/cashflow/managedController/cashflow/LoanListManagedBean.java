package com.spbd.cashflow.managedController.cashflow;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.spbd.cashflow.managedController.UserManagedBean;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.service.CashflowService;

@ManagedBean(name="loanListMB")
@ViewScoped
public class LoanListManagedBean implements Serializable{
	
    @ManagedProperty(value="#{CashflowService}")
    CashflowService cashflowService;
    
	@ManagedProperty(value="#{userMB}")
	UserManagedBean userMB;
    
    ArrayList<Cashflow> cashflows;
    
    ArrayList<Cashflow> filteredCashflows;

    @PostConstruct
	public void init() {
		cashflows = new ArrayList<Cashflow>();
		cashflows.addAll(cashflowService.getCashflows());
	}

	public ArrayList<Cashflow> getCashflows() {
		// change this to sql statements for better runtime
		if(userMB.getUserLoginRole().equalsIgnoreCase("admin")) {
			return cashflows;
		} else {
			ArrayList<Cashflow> cfList = new ArrayList<Cashflow>();
			for(Cashflow cf: cashflows)
				if(cf.getApplicant().getLoanOfficer().getUsername().equals(userMB.getUserLogin().getUsername()))
					cfList.add(cf);
			return cfList;
		}
	}

	public void setCashflows(ArrayList<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

	public CashflowService getCashflowService() {
		return cashflowService;
	}

	public void setCashflowService(CashflowService cashflowService) {
		this.cashflowService = cashflowService;
	}

	public ArrayList<Cashflow> getFilteredCashflows() {
		return filteredCashflows;
	}

	public void setFilteredCashflows(ArrayList<Cashflow> filteredCashflows) {
		this.filteredCashflows = filteredCashflows;
	}

	public UserManagedBean getUserMB() {
		return userMB;
	}

	public void setUserMB(UserManagedBean userMB) {
		this.userMB = userMB;
	}

}
