package com.spbd.cashflow.managedController;

import java.io.Serializable;

    public class LoanOfficerInfo implements Serializable,Comparable<LoanOfficerInfo>{
    	private String name;
 		int totalLoans=0;
 		double totalAmount=0;
 		
    	public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getTotalLoans() {
			return totalLoans;
		}
		public void setTotalLoans(int d) {
			this.totalLoans = d;
		}
		public double getTotalAmount() {
			return totalAmount;
		}
		void setTotalAmount(double d) {
			this.totalAmount = d;
		}

    	LoanOfficerInfo() {
    		
    	}
    	@Override
		public int compareTo(LoanOfficerInfo loi) {
			final int BEFORE = -1;
		    final int AFTER = 1;
			    if (this.totalAmount < loi.totalAmount) return AFTER;
			    else if (this.totalAmount > loi.totalAmount) return BEFORE;
			    else return 0;
		}
    }
