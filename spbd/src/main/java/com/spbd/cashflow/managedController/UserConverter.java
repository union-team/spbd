package com.spbd.cashflow.managedController;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserService;
 
 
@ManagedBean
@RequestScoped
public class UserConverter implements Converter {
	
	@ManagedProperty(value="#{UserService}")
    UserService service;
 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            return service.getUserById(Long.valueOf(value));
        }
        else {
            return null;
        }
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
        	return String.valueOf(((User) object).getId());
        }
        else {
            return null;
        }
    }

	public UserService getService() {
		return service;
	}

	public void setService(UserService service) {
		this.service = service;
	}   
}    