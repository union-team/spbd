package com.spbd.cashflow.managedController.report;

import com.spbd.cashflow.spring.model.Applicant;

	public class ApplicantData {
		public ApplicantData() {
			
		}
		Applicant applicant;
		double loanAmount = 0;
		int loans = 0;
	
		public Applicant getApplicant() {
			return applicant;
		}
		public void setApplicant(Applicant applicant) {
			this.applicant = applicant;
		}
		public double getLoanAmount() {
			return loanAmount;
		}
		public void setLoanAmount(double loanAmount) {
			this.loanAmount = loanAmount;
		}
		public int getLoans() {
			return loans;
		}
		public void setLoans(int loans) {
			this.loans = loans;
		}
	}