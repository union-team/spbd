package com.spbd.cashflow.managedController;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.mail.*;
import javax.mail.internet.*;

import org.springframework.beans.factory.annotation.Autowired;

import com.spbd.cashflow.spring.dao.UserLoginDAO;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserService;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


@ManagedBean(name = "SendEmailManagedBean")
@RequestScoped
public class SendEmailManagedBean implements Serializable{
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
			+ "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
			+ "(\\.[A-Za-z]{2,})$";

	@Autowired
	private UserLoginDAO UserLoginDao;

	@ManagedProperty(value = "#{UserService}")
	UserService userService;
	
	User user;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private String emailAddress;
	
	
	 String d_email = FacesContext.getCurrentInstance().getExternalContext().getInitParameter( "SentEmail" ),
	            d_password = FacesContext.getCurrentInstance().getExternalContext().getInitParameter( "EmailPassword" ), 
	            d_host = FacesContext.getCurrentInstance().getExternalContext().getInitParameter( "EmailHost" ),
	            d_port = FacesContext.getCurrentInstance().getExternalContext().getInitParameter( "EmailPort" ),
	            //m_to = "motasem_mask@yahoo.com", // Target email address
	            m_subject = "New password sent from SPBD",
	            m_text = "Hey, this is a test email.";
	    
	    public void SendEmail(String ToEmail) throws NoSuchAlgorithmException,
		UnsupportedEncodingException{
	    	
	    	User user = new User();
			
			user = userService.existEmail(ToEmail);
			this.setUser(user);
			
			String plainText = randomPassword(8);
			String encryptPassword = md5(plainText);
			
			user.setPassword(encryptPassword);
			
			getUserService().updateUser(user);
			
			 // message contains HTML markups        
			m_text = "<b><i>Dear " + user.getFirstName() + " " + user.getLastName() + ",</b></i><br>";        
			m_text += "<br>Here is your details:<br><br>";        
			m_text += "<font color=red>Username: " + user.getUsername() +
					 "<br> Password: "+ plainText + "</font><br><br><br>";
			m_text += "Please login and change your password.<br><br>";
			m_text += "<i>Best Regards,<br>SPBD Administrator.</i>";
			
			//m_text = "Dear " + user.getFirstName() + user.getLastName() + "</br> Here is your details: </br> Username: "+ user.getUsername() +
					 //"</br> Password: "+ plainText + "</br> Please login and change your password.";
	    	
	        Properties props = new Properties();
	        props.put("mail.smtp.user", d_email);
	        props.put("mail.smtp.host", d_host);
	        props.put("mail.smtp.port", d_port);
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        //props.put("mail.smtp.debug", "true");
	        props.put("mail.smtp.socketFactory.port", d_port);
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtp.socketFactory.fallback", "false");
	        try {
	            Authenticator auth = new SMTPAuthenticator();
	            Session session = Session.getInstance(props, auth);     
	            MimeMessage msg = new MimeMessage(session);
	            msg.setContent(m_text, "text/html; charset=utf-8");
	            //msg.setText(m_text);
	            msg.setSubject(m_subject);
	            msg.setFrom(new InternetAddress(d_email));
	            //msg.addRecipient(Message.RecipientType.TO, new InternetAddress(m_to));
	            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(ToEmail));
	            Transport.send(msg);
	            
	            
	            	setEmailAddress(null);
	 	            
	  	           this.showPanelNow(true);
	  	           this.HidePanelNow(false);
	  	           
	  	         //FacesContext.getCurrentInstance().getExternalContext().redirect("ServiceMail.xhtml");
	          
	            
	        } catch (Exception mex) {
	            mex.printStackTrace();
	        }
	        
	      	        
	    }
	   
	  	    
	    private class SMTPAuthenticator extends javax.mail.Authenticator {
	        public PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(d_email, d_password);
	        }
	    }


		public User getUser() {
			return user;
		}


		public void setUser(User user) {
			this.user = user;
		}


		public String getFirstName() {
			return firstName;
		}


		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}


		public String getLastName() {
			return lastName;
		}


		public void setLastName(String lastName) {
			this.lastName = lastName;
		}


		public String getUsername() {
			return username;
		}


		public void setUsername(String username) {
			this.username = username;
		}


		public String getPassword() {
			return password;
		}


		public void setPassword(String password) {
			this.password = password;
		}


		public String getEmailAddress() {
			return emailAddress;
		}


		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}
		
		public UserService getUserService() {
			return userService;
		}


		public void setUserService(UserService userService) {
			this.userService = userService;
		}
		
		// convert the password to hex format md5
		private String md5(String input) throws NoSuchAlgorithmException,
				UnsupportedEncodingException {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] md5hash = new byte[32];
			md.update(input.getBytes("iso-8859-1"), 0, input.length());
			md5hash = md.digest();
			return convertToHex(md5hash);
		}

		private static String convertToHex(byte[] b) {
			StringBuilder result = new StringBuilder(32);
			for (int i = 0; i < b.length; i++) {
				result.append(Integer.toString((b[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			System.out.println(result.toString());

			return result.toString();
		}
		
		private static String randomPassword(int value) {
			 
			StringBuilder password = new StringBuilder();
			 
			for (int i = 0; i < value; i++)
			 
			password.append(randomChar());
			 
			return password.toString();
			 
			}

			private static char randomChar() {
			 
			Random random = new Random();
			 
			int rand = random.nextInt(2);
			 
			if (rand == 1) {
			 
			int randVal = random.nextInt(26);
			 
			return (char) ('a' + randVal);
			 
			} else {
			 
			int randVal = random.nextInt(26);
			 
			return (char) ('A' + randVal);
			 
			}
			 
			}

	    
		// Email Address Validation

		public void ValidateEmailAddress(FacesContext context,
				UIComponent component, Object value) throws ValidatorException {
			
			String email = (String) value;

			if (value == null) {
				return;
			}

			/* check for email address valid format */

			Boolean Valid = email.matches(EMAIL_PATTERN);

			if (!Valid) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Invalid email format.", null));
			}

			/* check email address for duplicates in the database */

			if (userService.existEmail(email) == null) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Email is not stored in our database.",
						null));
			}
		}
		
		// Configure the panelGroup attributes
		
		private boolean showPanel = false;
		private boolean hidePanel = true;
		
		
		public void showPanelNow(boolean value)
		{
			this.setShowPanel(value);
			
		}
		
		public void HidePanelNow(boolean value)
		{
			this.setHidePanel(value);
			
		}
		
		
		// Setters and Getters
				
	    public boolean isShowPanel() {
			return showPanel;
		}

		public void setShowPanel(boolean showPanel) {
			this.showPanel = showPanel;
		}


		public boolean isHidePanel() {
			return hidePanel;
		}


		public void setHidePanel(boolean hidePanel) {
			this.hidePanel = hidePanel;
		}

		
	}