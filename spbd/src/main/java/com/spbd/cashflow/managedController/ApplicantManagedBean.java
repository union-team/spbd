package com.spbd.cashflow.managedController;
 
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.hibernate.Hibernate;

import com.spbd.cashflow.spring.dao.UserLoginDAO;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ApplicantService;
import com.spbd.cashflow.spring.service.UserService;

@ManagedBean(name="applicantMB")
@RequestScoped
public class ApplicantManagedBean implements Serializable {
 
	private static final Logger logger = Logger.getLogger(ApplicantManagedBean.class.getName());
	
    private static final long serialVersionUID = 1L;
    private static final String SUCCESS = "loans/addcashflow";
    private static final String ERROR   = "error";
 
    @ManagedProperty(value="#{ApplicantService}")
    transient ApplicantService applicantService;
    
    @ManagedProperty(value="#{UserService}")
    transient UserService userService;
    
	@ManagedProperty(value="#{userMB}")
	transient UserManagedBean userMB;
 
    List<Applicant> applicantList;
    
    private String id;
	private String name;
	private Date dob;
    private User loanOfficer;
    
    public String addApplicant() {
        try {
        	if(getApplicantService().getApplicantByName(getName(),getDob()) == null) {        		
        		Applicant applicant = new Applicant();
        		applicant.setName(getName());
        		applicant.setLoanOfficer(loanOfficer);
        		applicant.setDob(dob);
        		
        		getApplicantService().addApplicant(applicant);
        		
        		getUserService().addUserApplicant(loanOfficer, applicant);
        		
        		return SUCCESS;
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }   
 
        return ERROR;
    }
 
    public void reset() {
        this.setName("");
    }
    
    public ApplicantService getApplicantService() {
		return applicantService;
	}
	public void setApplicantService(ApplicantService applicantService) {
		this.applicantService = applicantService;
	}
	public List<Applicant> getApplicantList() {
		applicantList = new ArrayList<Applicant>();
		
		// change this to sql statements for better runtime
		List<Applicant> list = getApplicantService().getApplicants();
		if(userMB.getUserLoginRole().equalsIgnoreCase("admin")) {			
			applicantList.addAll(list);
		} else {
			List<Applicant> subList = new ArrayList<Applicant>();
			for(Applicant a : list)
				if(a.getLoanOfficer().getUsername().equals(userMB.getUserLogin().getUsername()))
					subList.add(a);
			return subList;
		}
		
		return applicantList;
	}
	public void setApplicantList(List<Applicant> applicantList) {
		this.applicantList = applicantList;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public User getLoanOfficer() {
		return loanOfficer;
	}
	public void setLoanOfficer(User loanOfficer) {
		this.loanOfficer = loanOfficer;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UserManagedBean getUserMB() {
		return userMB;
	}

	public void setUserMB(UserManagedBean userMB) {
		this.userMB = userMB;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	
	
}