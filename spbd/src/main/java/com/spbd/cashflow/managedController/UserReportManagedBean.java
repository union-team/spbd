package com.spbd.cashflow.managedController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.chart.MeterGaugeChartModel;

import com.spbd.cashflow.managedController.report.ApplicantData;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.UserReportService;

@RequestScoped
@ManagedBean(name = "userReportMB")
public class UserReportManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{UserReportService}")
	UserReportService userReportService;

	@ManagedProperty(value = "#{userMB}")
	UserManagedBean userManagedBean;
	
	User user;
	List<ApplicantData> applicantDataList;
	
	@PostConstruct
	public void init() {
		user = getUserManagedBean().getUserLogin();
		createMeterGaugeModels();
	}

	public double thisMonthLoanAmount;

	public double getThisMonthLoanAmount() {
		return thisMonthLoanAmount;
	}

	public void fetchApplicantData() {
		applicantDataList = new ArrayList<ApplicantData>();
		List<Applicant> appList = new ArrayList<Applicant>();
		appList = getApplicantList();
		for (Applicant a:appList) {
			ApplicantData app = new ApplicantData();
			app.setApplicant(a);
			for (Cashflow c:getUserReportService().getCashflowsByApplicant(a)) {
				if (c.getAccepted()==true) {
					app.setLoanAmount(app.getLoanAmount()+c.getLoanAmount());
					app.setLoans(app.getLoans()+1);
				}
			}
			applicantDataList.add(app);
		}
	}	
	public List<ApplicantData> getApplicantDataList() {
		fetchApplicantData();
		return applicantDataList;
	}

	public void setApplicantDataList(List<ApplicantData> applicantDataList) {
		this.applicantDataList = applicantDataList;
	}

	public void setThisMonthLoanAmount(double thisMonthLoanAmount) {
		this.thisMonthLoanAmount = thisMonthLoanAmount;
	}

	private MeterGaugeChartModel meterGaugeModel2;
	private List<Cashflow> cashflowList;
	private List<Applicant> applicantList;

	public List<Cashflow> getCashflowList() {
		getCashflows();
		return cashflowList;
	}

	public void setCashflowList(List<Cashflow> cashflowList) {
		this.cashflowList = cashflowList;
	}

	public List<Applicant> getApplicantList() {
		getApplicants();
		return applicantList;
	}

	public void setApplicantList(List<Applicant> applicantList) {
		this.applicantList = applicantList;
	}

	public MeterGaugeChartModel getMeterGaugeModel2() {
		return meterGaugeModel2;
	}

	private MeterGaugeChartModel initMeterGaugeModel() {

		List<Number> intervals = new ArrayList<Number>() {
			{
				int limit = (int) getUserReportService().totalLoanAmount(user);
				add(limit / 4);
				add(limit / 3);
				add(limit / 2);
				add(limit);
			}
		};

		return new MeterGaugeChartModel(totalLoanAmount(), intervals);
	}

	private void createMeterGaugeModels() {
		String tl = String.valueOf(totalLoanAmount());
		meterGaugeModel2 = initMeterGaugeModel();
		meterGaugeModel2.setTitle("Loans by "+user.getFirstName());
		meterGaugeModel2.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
		meterGaugeModel2.setGaugeLabel(tl);
		meterGaugeModel2.setGaugeLabelPosition("bottom");
		meterGaugeModel2.setShowTickLabels(false);
		meterGaugeModel2.setLabelHeightAdjust(110);
		meterGaugeModel2.setIntervalOuterRadius(200);
	}

	public void getApplicants() {
		applicantList = getUserReportService().applicantList(user);
	}

	public void getCashflows() {
		getApplicants();
		cashflowList = getUserReportService().cashflowList(applicantList);
	}

	public float totalLoanAmount() {
		getCashflows();
		float loanAmount = 0;
		for (Cashflow c : cashflowList) {
			if (c.getAccepted()==true)
				loanAmount += c.getLoanAmount();
		}
		return loanAmount;
	}

	public void thisMonthLoanAmount() {
		getCashflows();
		Date thisMonth = new Date();
		thisMonth.setDate(1);
		double loanAmount = 0;
		for (Cashflow c : cashflowList) {
			if (c.getDateSubmitted().after(thisMonth)
					&& c.getDateSubmitted().before(new Date())&&c.getAccepted()==true)
				loanAmount += c.getLoanAmount();
		}
		setThisMonthLoanAmount(loanAmount);
	}

	public UserReportService getUserReportService() {
		return userReportService;
	}

	public void setUserReportService(UserReportService userReportService) {
		this.userReportService = userReportService;
	}

	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}

	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
