package com.spbd.cashflow.managedController;

import java.io.Serializable;

public class Guideline implements Serializable{
	private String businessType;
	private double minNetCashflow;
	private double maxNetCashflow;
	
	public Guideline(String businessType, double minNetCashflow, double maxNetCashflow) {
		this.businessType = businessType;
		this.minNetCashflow = minNetCashflow;
		this.maxNetCashflow = maxNetCashflow;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public double getMinNetCashflow() {
		return minNetCashflow;
	}

	public void setMinNetCashflow(double minNetCashflow) {
		this.minNetCashflow = minNetCashflow;
	}

	public double getMaxNetCashflow() {
		return maxNetCashflow;
	}

	public void setMaxNetCashflow(double maxNetCashflow) {
		this.maxNetCashflow = maxNetCashflow;
	}
	
	
}
