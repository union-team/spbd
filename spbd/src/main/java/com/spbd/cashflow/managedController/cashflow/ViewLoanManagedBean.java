package com.spbd.cashflow.managedController.cashflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ExternalContext; 

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Averages;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.service.ApplicantService;
import com.spbd.cashflow.spring.service.CashflowService;

@ManagedBean(name="viewLoanMB")
@ViewScoped
public class ViewLoanManagedBean implements Serializable{
	
	@ManagedProperty(value="#{CashflowService}")
    CashflowService service;
	
	private Cashflow cashflow;
	private List<Averages> avgs;

	
	@PostConstruct
    public void init() {
		Map<String,String> params = (Map<String, String>)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String param = params.get("id");
		long parameter = Long.parseLong(param, 10);
		cashflow = service.getCashflowById(parameter);
		avgs = new ArrayList<Averages>();
		Averages temp = new Averages();
		temp.setAvgBusIn(cashflow.getDiaryBusinessInAverage());
		temp.setAvgBusOut(cashflow.getDiaryBusinessOutAverage());
		temp.setAvgHouseIn(cashflow.getDiaryHouseholdInAverage());
		temp.setAvgHouseOut(cashflow.getDiaryHouseholdOutAverage());
		avgs.add(temp);
		
    }

	public CashflowService getService() {
		return service;
	}

	public void setService(CashflowService service) {
		this.service = service;
	}
	
	public Cashflow getCashflow(){
		return cashflow;
	}
	
	public void setCashflow(Cashflow cashflow) {
		this.cashflow = cashflow;
	}
	
	public List<Averages> getAvgs(){
		return avgs;
	}
	
	public void setAvgs(List<Averages> avgs){
		this.avgs = avgs;
	}
	

	
}