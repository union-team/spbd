package com.spbd.cashflow.managedController;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.LinearAxis;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.chart.PieChartModel;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ReportService;
import com.spbd.cashflow.managedController.LoanOfficerInfo;
import com.spbd.cashflow.managedController.report.ApplicantData;
import com.spbd.cashflow.managedController.report.MonthlyReport;
@SessionScoped
@ManagedBean(name = "reportMB")
public class ReportManagedBean implements Serializable {
	
    private static final long serialVersionUID = 1L;
 
    @ManagedProperty(value="#{ReportService}")
    transient ReportService reportService;

	@PostConstruct
	public void init() {
		createMeterGaugeModels();
		createMultiAxisModel();
		createAnimatedModels();
        endDateLoans = new Date();
        startDateLoans = new Date();
        createPieModels();

	}

	List<Applicant> applicantList;
	List<User> userList;
	List<Applicant> applicantByUser;
	List<Cashflow> cashflowByUser;
	List<Cashflow> cashflowList;
	List<ApplicantData> applicantDataList;

	private Date startDateLoans = null;
    private Date endDateLoans = null;
    private PieChartModel pieModel1;
    
    private void createPieModels() {
        createPieModel1(null);
    }
    
    
    public void createPieModel1(ActionEvent event) {
 
    	float periodicLoanAmount = 0;
    	showTotalLoanAmount();
    	getCashflowList();

    	for (Cashflow c:cashflowList) {
    			if (c.getDateSubmitted().after(startDateLoans)&&c.getDateSubmitted().before(endDateLoans)&&c.getAccepted()==true)
    				periodicLoanAmount += c.getLoanAmount();
    	}
    	int restOfLoans = (int)getTotalLoanAmount() - (int)periodicLoanAmount;
        pieModel1 = new PieChartModel();
         
        pieModel1.set("Amount in this period: "+periodicLoanAmount, periodicLoanAmount);
        pieModel1.set("Rest of loans: "+restOfLoans, restOfLoans);
         
        pieModel1.setTitle("Loan Amounts");
        pieModel1.setLegendPosition("w");
    }
    

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public Date getStartDateLoans() {
		return startDateLoans;
	}
	public void setStartDateLoans(Date startDateLoans) {
		this.startDateLoans = startDateLoans;
	}
	public Date getEndDateLoans() {
		return endDateLoans;
	}
	public void setEndDateLoans(Date endDateLoans) {
		this.endDateLoans = endDateLoans;
	}
		
	public List<Cashflow> getCashflowList() {
		cashflowList = getReportService().getCashflows();
		return cashflowList;
	}

	public void setCashflowList(List<Cashflow> cashflowList) {
		this.cashflowList = cashflowList;
	}

	List<LoanOfficerInfo> officerInfoList;
	List<User> topOfficers;
	List<MonthlyReport> pastThreeMonthsLoan;

	Date endDate0 = new Date();
	Date startDate0 = MonthlyReport.subtractMonth(endDate0);
	double loanAmount0 = 0;
	Date startDate1 = MonthlyReport.subtractMonth(startDate0);
	Date endDate1 = MonthlyReport.subtractMonth(endDate0);
	double loanAmount1 = 0;
	Date startDate2 = MonthlyReport.subtractMonth(startDate1);
	Date endDate2 = MonthlyReport.subtractMonth(endDate1);
	double loanAmount2 = 0;

	public void showPastThreeMonthsRecords() {
		getCashflowList();
		for (Cashflow c : cashflowList) {
			if (c.getAccepted()==true) {
				if (c.getDateSubmitted().after(startDate0)
						&& c.getDateSubmitted().before(endDate0))
					loanAmount0 += c.getLoanAmount();
				else if (c.getDateSubmitted().after(startDate1)
						&& c.getDateSubmitted().before(endDate1))
					loanAmount1 += c.getLoanAmount();
				else if (c.getDateSubmitted().after(startDate2)
						&& c.getDateSubmitted().before(endDate2))
					loanAmount2 += c.getLoanAmount();
			}
		}
	}

	public List<MonthlyReport> getPastThreeMonthsLoan() {
		showPastThreeMonthsRecords();
		getCashflowList();
		return pastThreeMonthsLoan;
	}

	public void setPastThreeMonthsLoan(List<MonthlyReport> pastThreeMonthsLoan) {
		this.pastThreeMonthsLoan = pastThreeMonthsLoan;
	}

	private User user = new User();
	private Applicant applicant = new Applicant();
	private Diary diary = new Diary();
	private Cashflow cashflow = new Cashflow();
	private LoanOfficerInfo loanOfficerInfo = new LoanOfficerInfo();

	public LoanOfficerInfo getLoanOfficerInfo() {
		return loanOfficerInfo;
	}

	public void setLoanOfficerInfo(LoanOfficerInfo loanOfficerInfo) {
		this.loanOfficerInfo = loanOfficerInfo;
	}

	private int loanAmount;
	private float totalLoanAmount;

	public void showTotalLoanAmount() {
		totalLoanAmount = getReportService().getTotalLoanAmount();
	}

	public float getTotalLoanAmount() {
		return this.totalLoanAmount;
	}

	public void setTotalLoanAmount(int totalLoanAmount) {
		this.totalLoanAmount = totalLoanAmount;
	}

	public void showApplicantsByUser(ActionEvent event) {
		applicantByUser = getReportService().getApplicantsByLoanOfficer(user);
	}

	public void showCashflowsByUser(ActionEvent event) {
		cashflowByUser = getReportService().getCashflowByUser(user);
	}

	public void showApplicantList() {
		applicantList = getReportService().getApplicantList();
	}
	
	public void fetchApplicantData() {
		applicantDataList = new ArrayList<ApplicantData>();
		List<Applicant> appList = getApplicantList();
		for (Applicant a:appList) {
			ApplicantData app = new ApplicantData();
			app.setApplicant(a);
			for (Cashflow c:getReportService().getCashflowsByApplicant(a)) {
				if (c.getAccepted()==true) {
					app.setLoanAmount(app.getLoanAmount()+c.getLoanAmount());
					app.setLoans(app.getLoans()+1);
				}
			}
			applicantDataList.add(app);
		}
	}	

	public void showLoanInfo() {

		userList = getUserList();
		officerInfoList = new ArrayList<LoanOfficerInfo>();
		for (User user : userList) {
			loanOfficerInfo = new LoanOfficerInfo();
			loanOfficerInfo.setName(user.getFirstName() + " "
					+ user.getLastName());
			setUser(user);
			showCashflowsByUser(null);
			for (Cashflow cashflow : cashflowByUser) {
				if (cashflow.getAccepted()==true) {
					loanOfficerInfo.setTotalAmount(loanOfficerInfo.getTotalAmount()
							+ cashflow.getLoanAmount());
					loanOfficerInfo
							.setTotalLoans(loanOfficerInfo.getTotalLoans() + 1);
				}
			}
			officerInfoList.add(loanOfficerInfo);

		}
	}

	public double getLoanAmount0() {
		return loanAmount0;
	}

	public void setLoanAmount0(double loanAmount0) {
		this.loanAmount0 = loanAmount0;
	}

	public double getLoanAmount1() {
		return loanAmount1;
	}

	public void setLoanAmount1(double loanAmount1) {
		this.loanAmount1 = loanAmount1;
	}

	public double getLoanAmount2() {
		return loanAmount2;
	}

	public void setLoanAmount2(double loanAmount2) {
		this.loanAmount2 = loanAmount2;
	}

	public List<LoanOfficerInfo> getOfficerInfoList() {
		return officerInfoList;
	}

	public void setOfficerInfoList(List<LoanOfficerInfo> officerInfoList) {
		this.officerInfoList = officerInfoList;
	}

	public int getLoanAmount() {
		return this.loanAmount;
	}

	public void setLoanAmount(int loanAmount) {
		this.loanAmount = loanAmount;
	}

	public List<Applicant> getApplicantList() {
		showApplicantList();
		return this.applicantList;
	}

	public List<Applicant> getApplicantByUser() {
		// return getReportService().getApplicantsByLoanOfficer(user);
		return applicantByUser;
	}

	public List<User> getUserList() {
		userList = new ArrayList<User>();
		userList.addAll(getReportService().getUserList());
		return userList;
	}

	public List<Cashflow> getCashflowByUser() {
		// cashflowByUser = new ArrayList<Cashflow>();
		// cashflowByUser.addAll(getReportService().getCashflowByUser(user));
		// return cashflowByUser;
		return this.cashflowByUser;
	}

	// charts

	private void createAnimatedModels() {

		animatedModel2 = initBarModel();
		animatedModel2.setTitle("Last three months reports");
		animatedModel2.setAnimate(true);
		animatedModel2.setLegendPosition("ne");
		Axis yAxis = animatedModel2.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(15000);
	}

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();

		ChartSeries monthlyLoans = new ChartSeries();
		monthlyLoans.setLabel("Loan Amount");
		showPastThreeMonthsRecords();
		monthlyLoans.set(
				new SimpleDateFormat("MMM").format(startDate0) + " "
						+ startDate0.getDay() + "-"
						+ new SimpleDateFormat("MMM").format(endDate0) + " "
						+ endDate0.getDay(), loanAmount0);
		monthlyLoans.set(
				new SimpleDateFormat("MMM").format(startDate1) + " "
						+ startDate1.getDay() + "-"
						+ new SimpleDateFormat("MMM").format(endDate1) + " "
						+ endDate1.getDay(), loanAmount1);
		monthlyLoans.set(
				new SimpleDateFormat("MMM").format(startDate2) + " "
						+ startDate2.getDay() + "-"
						+ new SimpleDateFormat("MMM").format(endDate2) + " "
						+ endDate2.getDay(), loanAmount2);

		model.addSeries(monthlyLoans);
		return model;
	}

	private MeterGaugeChartModel meterGaugeModel2;
	private BarChartModel animatedModel2;
	private LineChartModel multiAxisModel;

	public MeterGaugeChartModel getMeterGaugeModel2() {
		return meterGaugeModel2;
	}

	private MeterGaugeChartModel initMeterGaugeModel() {
		List<Number> intervals = new ArrayList<Number>() {
			{
				add(8000);
				add(20000);
				add(48000);
				add(88000);
			}
		};

		return new MeterGaugeChartModel(getTotalLoanAmount(), intervals);
	}

	private void createMeterGaugeModels() {
		showTotalLoanAmount();
		Integer s = (int) getTotalLoanAmount();
		String tl = s.toString();
		meterGaugeModel2 = initMeterGaugeModel();
		meterGaugeModel2.setTitle("Total Loans");
		meterGaugeModel2.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
		meterGaugeModel2.setGaugeLabel(tl + " $");
		meterGaugeModel2.setGaugeLabelPosition("bottom");
		meterGaugeModel2.setShowTickLabels(false);
		meterGaugeModel2.setLabelHeightAdjust(20);
		meterGaugeModel2.setIntervalOuterRadius(180);
	}

	public BarChartModel getAnimatedModel2() {
		return animatedModel2;
	}

	public LineChartModel getMultiAxisModel() {
		return multiAxisModel;
	}

	private void createMultiAxisModel() {
		multiAxisModel = new LineChartModel();

		BarChartSeries loanAmounts = new BarChartSeries();
		loanAmounts.setLabel("Loan Amounts");

		LineChartSeries numberOfLoans = new LineChartSeries();
		numberOfLoans.setLabel("Number of Loans");
		numberOfLoans.setXaxis(AxisType.X2);
		numberOfLoans.setYaxis(AxisType.Y2);

		showLoanInfo();
		Collections.sort(officerInfoList);
		int max;
		if (officerInfoList.size() > 7)
			max = 7;
		else
			max = officerInfoList.size();
		List<LoanOfficerInfo> topOfficers = officerInfoList.subList(0, max);

		for (LoanOfficerInfo loi : topOfficers) {
			numberOfLoans.set(loi.getName(), loi.getTotalLoans());
			loanAmounts.set(loi.getName(), loi.getTotalAmount());
		}

		multiAxisModel.addSeries(loanAmounts);
		multiAxisModel.addSeries(numberOfLoans);

		multiAxisModel.setTitle("Active Loan Officers");
		multiAxisModel.setMouseoverHighlight(false);

		multiAxisModel.getAxes().put(AxisType.X,
				new CategoryAxis("Loan Amounts"));
		multiAxisModel.getAxes().put(AxisType.X2,
				new CategoryAxis("Number of Loans"));

		Axis yAxis = multiAxisModel.getAxis(AxisType.Y);
		yAxis.setLabel("Amount");
		yAxis.setMin(0);
		yAxis.setMax(100000);

		Axis y2Axis = new LinearAxis("No of loans");
		y2Axis.setMin(0);
		y2Axis.setMax(100);

		multiAxisModel.getAxes().put(AxisType.Y2, y2Axis);
	}


	public void postProcessXLS(Object document) {
		HSSFWorkbook wb = (HSSFWorkbook) document;
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow header = sheet.getRow(0);

		HSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
			HSSFCell cell = header.getCell(i);
			cell.setCellStyle(cellStyle);
		}
	}

	public void preProcessPDF(Object document) throws IOException,
			BadElementException, DocumentException {
		Document pdf = (Document) document;
		pdf.open();
		pdf.setPageSize(PageSize.A4);
	}

	// Setters and Getters
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public void setCashflowByUser(List<Cashflow> cashflowByUser) {
		this.cashflowByUser = cashflowByUser;
	}

	public void setApplicantByUser(List<Applicant> applicantByUser) {
		this.applicantByUser = applicantByUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User loanOfficer) {
		this.user = loanOfficer;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public Diary getDiary() {
		return diary;
	}

	public void setDiary(Diary diary) {
		this.diary = diary;
	}

	public Cashflow getCashflow() {
		return cashflow;
	}

	public void setCashflow(Cashflow cashflow) {
		this.cashflow = cashflow;
	}

	public void setApplicantList(List<Applicant> applicantList) {
		this.applicantList = applicantList;
	}


    public List<ApplicantData> getApplicantDataList() {
		fetchApplicantData();
		return applicantDataList;
	}


	public void setApplicantDataList(List<ApplicantData> applicantDataList) {
		this.applicantDataList = applicantDataList;
	}
	
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public ReportService getReportService() {
		return reportService;
	}

    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
}
