package com.spbd.cashflow.spring.model;

public class Averages {
	private double avgBusIn;
    private double avgBusOut;
    private double avgHouseIn;
    private double avgHouseOut;
    
    public Averages(){
    }
    
    public double getAvgBusIn() {
 	   return avgBusIn;
    }
    
    public void setAvgBusIn(double avgBusIn) {
 	   this.avgBusIn = avgBusIn;
    }
    
    public double getAvgBusOut() {
 	   return avgBusOut;
    }
    
    public void setAvgBusOut(double avgBusOut) {
 	   this.avgBusOut = avgBusOut;
    }
    
    public double getAvgHouseIn() {
 	   return avgHouseIn;
    }
    
    public void setAvgHouseIn(double avgHouseIn) {
 	   this.avgHouseIn = avgHouseIn;
    }
    
    public double getAvgHouseOut() {
 	   return avgHouseOut;
    }
    
    public void setAvgHouseOut(double avgHouseOut) {
 	   this.avgHouseOut = avgHouseOut;
    }
}