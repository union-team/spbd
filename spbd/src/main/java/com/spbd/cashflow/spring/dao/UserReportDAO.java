package com.spbd.cashflow.spring.dao;
 

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.calculate.Criteria;
import com.spbd.cashflow.managedController.report.MonthlyReport;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;

@Repository
public class UserReportDAO  {
	
	@Autowired
    private SessionFactory sessionFactory;


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public List<Applicant> applicantList(User user){
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Applicant where loanOfficer = :user");
    	query.setParameter("user", user);
    	return query.list();
    }
    public List<Cashflow> cashflowList(List<Applicant> applicantList) {
    	if (applicantList.isEmpty())
    		return new ArrayList<Cashflow>();
    	else {
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow where applicant in (:applicantList)");
    	query.setParameterList("applicantList", applicantList);
    	return query.list();
    	}
    }
    public float totalLoanAmount(User user) {
    	float totalLoanAmount = 0;
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow");
    	List<Cashflow> cList = query.list();
    	for (Cashflow c: cList) {
    		totalLoanAmount += c.getLoanAmount();
    	}
    	return totalLoanAmount;
    }

    public List<Cashflow> getCashflowsByApplicants(Applicant applicant) {
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow c where c.applicant = :applicant and c.accepted = (1)");
    	query.setParameter("applicant", applicant);
    	return query.list();
    }
    
}