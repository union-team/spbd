package com.spbd.cashflow.spring.dao;
 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;

@Repository
public class ApplicantDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public void addApplicant(Applicant applicant) {
        getSessionFactory().getCurrentSession().save(applicant);
    }
   
    public void deleteApplicant(Applicant applicant) {
        getSessionFactory().getCurrentSession().delete(applicant);
    }
   
    public void updateApplicant(Applicant applicant) {
      getSessionFactory().getCurrentSession().update(applicant);
    }

    public List<Applicant> getApplicants() {
        return getSessionFactory().getCurrentSession().createQuery("from Applicant").list();
    }
    
    public Applicant getApplicantById(long id) {
        return (Applicant) getSessionFactory().getCurrentSession().get(Applicant.class, id);
    }

	public void addApplicantCashflow(Applicant applicant, Cashflow cashflow) {
		getSessionFactory().getCurrentSession().refresh(applicant);
		Hibernate.initialize(applicant.getCashflows());
		applicant.getCashflows().add(cashflow);
		getSessionFactory().getCurrentSession().update(applicant);
	}
	
	public Applicant getApplicantByName(String name, Date dob) {
		if(name==null || dob==null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dobstring=sdf.format(dob);
	        return (Applicant) getSessionFactory().getCurrentSession().createQuery("from Applicant u where u.name = '"+ name +"' and dob = '"+sdf.format(dob)+"'").uniqueResult();
	}
 
}