package com.spbd.cashflow.spring.service;

import java.io.Serializable;
import java.util.List;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spbd.cashflow.spring.dao.ReportDAO;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;

@Service("ReportService")
@Transactional(readOnly = true)
public class ReportService {

	@Autowired
	ReportDAO ReportDAO;

	@Transactional(readOnly = true)
	public List<Applicant> getApplicantsByLoanOfficer(User user) {
		return getReportDAO().getApplicantsByLoanOfficer(user);
	}

	@Transactional(readOnly = true)
	public List<Cashflow> getCashflowByUser(User user) {
		return getReportDAO().getCashflowsByLoanOfficer(user);
	}

	@Transactional(readOnly = true)
	public List<Cashflow> getCashflowsByMonth(Date start, Date end) {
		return getReportDAO().getCashflowsByDate(start, end);
	}

	@Transactional(readOnly = true) 
	public List<Cashflow> getCashflowsByApplicant (Applicant applicant){
		return getReportDAO().getCashflowsByApplicants(applicant);
	}
	
	public List<Object[]> getLoanOfficerLoanAmount() {
		return getReportDAO().getLoanOfficerLoanAmount();
	}
	
	public List<User> getActiveLoanOfficers(int number) {
		return getReportDAO().getActiveLoanOfficers(number);
	}
	
	public List<Cashflow> getCashflows() {
		return getReportDAO().getCashflows();
	}

	// public List<MonthlyReport> getLoanAmountForThePastThreeMonths() {
	// return getReportDAO().getLoanAmountForThePastThreeMonths();
	// }
	
	public List<Applicant> getApplicantList() {
		return getReportDAO().getApplicants();
	}
	
	public List<User> getUserList() {
		return getReportDAO().getLoanOfficers();
	}
	
	@Transactional(readOnly = true)
	public User getUserById(long id) {
		return getReportDAO().getUserById(id);
	}
	
	@Transactional(readOnly = true)
	public int getLoanNumber(User user) {
		return getReportDAO().getLoanNumber(user);
	}

	public ReportDAO getReportDAO() {
		return ReportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.ReportDAO = reportDAO;
	}

	public int getTotalLoanAmount() {
		return getReportDAO().getTotalLoanAmount();
	}

}
