package com.spbd.cashflow.spring.dao;
 
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;

@Repository
public class CashflowDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public long addCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().save(cashflow);
        return cashflow.getId();
    }
   
    public void deleteCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().delete(cashflow);
    }
   
    public void updateCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().update(cashflow);
    }

    public List<Cashflow> getCashflows() {
        List list = getSessionFactory().getCurrentSession().createQuery("from Cashflow").list();
        return list;
    }
    
    public Cashflow getCashflowById(long id) {
        Cashflow cf = (Cashflow) getSessionFactory().getCurrentSession().get(Cashflow.class, id);
        Hibernate.initialize(cf.getDiaries());
        return cf;
    }
 
}