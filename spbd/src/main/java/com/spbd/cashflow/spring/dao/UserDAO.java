package com.spbd.cashflow.spring.dao;
 
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.model.UsersAndRoles;

@Repository
public class UserDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public void addUser(User user) {
        getSessionFactory().getCurrentSession().save(user);
    }
   
	public void addUsersAndRoles(UsersAndRoles userANDroles) {
		getSessionFactory().getCurrentSession().save(userANDroles);
		
	}
	
    public void deleteUser(User user) {
        getSessionFactory().getCurrentSession().delete(user);
    }
    
    public void deleteUsersAndRoles(UsersAndRoles userANDroles) {
        getSessionFactory().getCurrentSession().delete(userANDroles);
    }
   
    public void updateUser(User user) {
      getSessionFactory().getCurrentSession().update(user);
    }
    
    public void updateUsersAndRoles(UsersAndRoles userANDroles) {
        getSessionFactory().getCurrentSession().update(userANDroles);
      }
    
    public void addUserApplicant(User user, Applicant applicant) {
    	getSessionFactory().getCurrentSession().refresh(user);
		Hibernate.initialize(user.getApplicants());
		user.getApplicants().add(applicant);
		getSessionFactory().getCurrentSession().update(user);
    }

    public List<User> getUsers() {
        return getSessionFactory().getCurrentSession().createQuery("from User").list();
    }
    
    public User getUserById(long id) {
        return (User) getSessionFactory().getCurrentSession().get(User.class, id);
    }
    
    public User getUserByUserName(String username) {
    	
     return (User) getSessionFactory().getCurrentSession().createQuery("from User u where u.username = '"+ username +"'").uniqueResult();
    }
    
    public User getUpdateUserByUserName(long id, String username) {
    	
        return (User) getSessionFactory().getCurrentSession().createQuery("from User u where u.username = '"+ username +"' and u.id !='"+ id + "'").uniqueResult();
       }
    
    public User existEmail(String email) {
    	
        return (User) getSessionFactory().getCurrentSession().createQuery("from User u where u.emailAddress = '"+ email +"'").uniqueResult();
       }
    
    public User existUpdateEmail(long id, String email) {
    	
        return (User) getSessionFactory().getCurrentSession().createQuery("from User u where u.emailAddress = '"+ email +"' and u.id !='"+ id + "'").uniqueResult();
       }
    
    public UsersAndRoles getUsersAndRolesById(long id) {
        return (UsersAndRoles) getSessionFactory().getCurrentSession().createQuery("from UsersAndRoles r where r.id = '"+ id +"'").uniqueResult();
    }

 
}