package com.spbd.cashflow.spring.service;

import java.util.List;
import java.util.Date;

import com.spbd.cashflow.spring.dao.UserReportDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spbd.cashflow.managedController.report.MonthlyReport;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;

@Service("UserReportService")
@Transactional(readOnly = true)
public class UserReportService {

	@Autowired
	UserReportDAO UserReportDAO;


	@Transactional(readOnly = true) 
	public List<Cashflow> getCashflowsByApplicant (Applicant applicant){
		return getUserReportDAO().getCashflowsByApplicants(applicant);
	}
	
	@Transactional(readOnly = true)
	public List<Cashflow> cashflowList(List<Applicant> applicantList) {
		return getUserReportDAO().cashflowList(applicantList);
	}

	public List<Applicant> applicantList(User user) {
		return getUserReportDAO().applicantList(user);
	}

	public float totalLoanAmount(User user) {
		return getUserReportDAO().totalLoanAmount(user);
	}

	public UserReportDAO getUserReportDAO() {
		return UserReportDAO;
	}

	public void setUserReportDAO(UserReportDAO userReportDAO) {
		UserReportDAO = userReportDAO;
	}
}