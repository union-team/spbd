package com.spbd.cashflow.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spbd.cashflow.spring.model.User;


@Controller
public class LoginController {
	
	@RequestMapping("/login.jsp")
	public String doLogin(){
		return "login.jsp";
	}
		
}
