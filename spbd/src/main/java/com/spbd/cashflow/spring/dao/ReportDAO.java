package com.spbd.cashflow.spring.dao;
 

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.managedController.report.MonthlyReport;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.User;

@Repository
public class ReportDAO  {
	
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    //ok
	public List<User> getLoanOfficers() {
		return getSessionFactory().getCurrentSession().createQuery("from User").list();
	}
	//ok
	public List<Applicant> getApplicants() {
		return getSessionFactory().getCurrentSession().createQuery("from Applicant").list();
	}
	//ok
	public List<Cashflow> getCashflowsByLoanOfficer(User user) {
		Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow where applicant.id in (select id  from Applicant where user_id= :userid)");
		query.setParameter("userid", user.getId());
		return query.list();
	}
	//ok
    public List<Cashflow> getCashflowsByApplicants(Applicant applicant) {
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow where applicant = :applicant");
    	query.setParameter("applicant", applicant);
    	return query.list();
    }
    
    public List<Applicant> getApplicantsByLoanOfficer(User user) {
    Query query=getSessionFactory().getCurrentSession().createQuery("from Applicant where user_id=:userid");
    query.setParameter("userid", user.getId());
    return query.list();
    }
    public Integer getTotalLoanAmount() {
    	int sum = 0;
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow c where c.accepted = (1)");
    	List<Cashflow> list = query.list();
    	for (Cashflow x:list) {
    		sum +=x.getLoanAmount();
    	}
    	return sum;
    }
    public List<Cashflow> getCashflows() {
    	return getSessionFactory().getCurrentSession().createQuery("from Cashflow c").list();
    }
    
	public List<Cashflow> getCashflowsByDate(Date start, Date end) {
		Query query = getSessionFactory().getCurrentSession().createQuery("From Cashflow where date_submitted >= :start and date_submitted<= :end");
		query.setParameter("start", start);
		query.setParameter("end", end);
		return query.list();
	}

	public List<User> getActiveLoanOfficers(int number) {
	return getSessionFactory().getCurrentSession().createQuery("from Cashflow C " + 
	"Join Applicant A on group by A.user_id order by sum(c.loan_amount)").setMaxResults(number).list();
	}
	
	public List<Object[]> getLoanOfficerLoanAmount() {
	Query query = getSessionFactory().getCurrentSession().createQuery("from Cashflow as c join c.applicant as a");
	//query.setParameter("userid", user.getId());
	return query.list();
	}

    public User getUserById(long id) {
        return (User) getSessionFactory().getCurrentSession().get(User.class, id);
    }
    
	public int getLoanNumber(User user) {
		Query query = getSessionFactory().getCurrentSession().createQuery("select Count(*) from Cashflow C Join Applicant A on C.applicant" +
				"= A.id where A.user_id = :userid group by A.user_id");
		query.setParameter("userid", user.getId());
		return Integer.valueOf(query.toString());
	}
}