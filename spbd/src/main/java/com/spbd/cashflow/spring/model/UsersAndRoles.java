package com.spbd.cashflow.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="UsersAndRoles")
public class UsersAndRoles implements Serializable{
	
	@Id
	@Column(name="id")
	private long id;
	
	@Id
	@Column(name="role_id")
	private int role_id;

	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	

}
