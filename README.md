**SPBD**

Welcome to the SPBD Cash Flow Appraisal Application!

The SPBD Cash Flow Appraisal Application is both an Android mobile app and a website providing loan appraisal solutions for South Pacific Business Development (SPBD) Microfinance Ltd. Users can enter their cash flow data and find the risk factor for their requested loan. The SPBD applications facilitate the processing, retrieving, and analysing of loan applications in remote areas.

Some of the features of the SPBD Application include:

    Enter a new loan application based on past and expected cash flows
    Summary of risk factors and advice on whether or not to accept the loan
    Charts and graphs based on loan applications and Centre Managers showing productivity
    Authentication and security to keep data private

The **SPBD** homepage is: 

http://www.spbdmicrofinance.com/

**Summary of setup
**

The distribution version of this application is available on the BitBucket repository and you can either clone: * git clone https://bitbucket.org/union-team/spbd.git or use the download link at the top left of our BitBucket overview page.

To deploy a copy you need to have Tomcat, MySQL, Maven, and Java running on your machine. First of all, after you download the application on your machine you need to create your MySQL database schema. create a file called database.properties and save it in `"/src/main/resources"`. The file should contain your database login information in the following template.

	jdbc.driverClassName=com.mysql.jdbc.Driver jdbc.dialect=org.hibernate.dialect.MySQLDialect jdbc.databaseurl=jdbc:mysql://[yourdbadress:yourdbport]/spbd_cashflow?autoReconnect=true jdbc.username=[yourdbusername] jdbc.password=[yourdbpassword]

Then open `/src/main/webapp/WEB-INF/applicationContext.xml` and enter your schema name under the property called "*hibernateProperties*". Note that for the first time you need to set the value for "*hibernate.hbm2ddl.auto*" to create to let the application create the tables and after that you should change the property to update. Now that you already set your database, you have to download the dependencies using maven so that your project can get what it needs and run on your machine. For that you should go to the application root using command line and run the following command:

	mvn install

After maven is done your application is almost deployed.

The last step is to run the database initialization script and create the first user to be able to login to the system.


Your system is now ready to use. You can login with the default account with admin for the both username and password and use the system.

to run the system you should navigate to your project root using command line and run the command:

	mvn tomcat:run

You can now access the web portal using this address:

`http://[yourserver]/cashflow
`

Now you can compile the android app using eclipse and android SDK or just download the compiled apk file from this URL:

	https://drive.google.com/file/d/0BzE3XfWxJZSbczdZOTNpRmZHVVU/view?usp=sharing

Install it on your android device, login as the users added from the web portal and enter your cashflows to the system.



This application is developed in October 2014 by a group of students from The University of Sydney as a charitable project for SPBD.

All the functions of this project are tested using JUnit and Mockito. JUnit tests can be found in the BitBucket repository.

User interface is also tested with Selenium IDE.

Please find more information about contributing to the project in the Wiki.

Enjoy and please send us your feedback! The SPBD development team.
